package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class WebDriverController {

    private static Logger logger = LogManager.getLogger(WebDriverController.class);
    private static final String driverDir = System.getProperty("user.dir") + "\\src\\main\\resources\\drivers";

    static {
        System.setProperty("webdriver.chrome.driver", driverDir + "\\chromedriver.exe");
        System.setProperty("webdriver.ie.driver", driverDir + "\\IEDriverServer.exe");
        System.setProperty("webdriver.edge.driver", driverDir + "\\msedgedriver.exe");
    }

    private static WebDriverController instance = new WebDriverController();
    private static final ThreadLocal<WebDriver> WEB_DRIVER_THREAD_LOCAL = new InheritableThreadLocal<>();
    private static final String DOWNLOAD_FILE_PATH = System.getProperty("user.dir") + "\\Downloads";

    private WebDriverController() {
    }

    public static WebDriverController getWebDriverControllerInstance() {
        if (instance == null) {
            instance = new WebDriverController();
            logger.info("Creating WebDriver controller instance");
        }
        return instance;
    }

    public void setDriver() {
        WebDriver driver;
        switch (LoadProperties.getProperties().getProperty("browser")) {
            case "internetExplorer":
                InternetExplorerOptions options = new InternetExplorerOptions();
                options.introduceFlakinessByIgnoringSecurityDomains();
                options.destructivelyEnsureCleanSession();
                options.enablePersistentHovering();
                options.ignoreZoomSettings();
                options.requireWindowFocus();
                //options.disableNativeEvents();
                driver = new InternetExplorerDriver(options);
                break;
            case "edge":
                EdgeOptions edgeOptions = new EdgeOptions();
                Map<String, Object> prefs2 = new HashMap<String, Object>();
                prefs2.put("credentials_enable_service", false);
                prefs2.put("profile.password_manager_enabled", false);
                prefs2.put("profile.default_content_settings.popups", 0);
                prefs2.put("download.default_directory", DOWNLOAD_FILE_PATH);
                edgeOptions.setExperimentalOption("prefs", prefs2);
                edgeOptions.setExperimentalOption("useAutomationExtension", false);
                edgeOptions.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
                driver= new EdgeDriver(edgeOptions);
                break;
            default:
                //WebDriverManager.globalConfig().setProxy("webproxysp.wip.services::.local:8080");

              //  chromedriver().setup();
                WebDriverManager.chromedriver().setup();
                ChromeOptions options2 = new ChromeOptions();
                options2.setExperimentalOption("useAutomationExtension", false);
                options2.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
                //options2.addArguments("--no-sandbox", "--disable-dev-shm-usage", "--headless");
                Map<String, Object> prefs = new HashMap<String, Object>();
                prefs.put("credentials_enable_service", false);
                prefs.put("profile.password_manager_enabled", false);
                prefs.put("profile.default_content_settings.popups", 0);
                prefs.put("download.default_directory", DOWNLOAD_FILE_PATH);
                /*prefs.put("download.prompt_for_download", false);
                prefs.put("plugins.always_open_pdf_externally", true);*/
                options2.setExperimentalOption("prefs", prefs);

                driver = new ChromeDriver(options2);
        }
        driver.manage().timeouts().implicitlyWait(Long.parseLong(LoadProperties.getProperties().getProperty("script.timeout")), TimeUnit.SECONDS);
        driver.manage().window().maximize();
        WEB_DRIVER_THREAD_LOCAL.set(driver);
    }

    public WebDriver getDriver() {
        return WEB_DRIVER_THREAD_LOCAL.get();
    }
}
