package pages;

import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import pages.components.QueryWidgetPopup;
import pages.components.ScreeningWidgetPopup;
import pages.components.WebAppAttributeTable;
import pages.components.WebAppLayerMenuOptions;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static utils.Utils.getCountOfRecordsInTheLatestDownloadedFile;
import static utils.Utils.round;

public class WebAppPage extends HeaderPage implements WebAppLayerMenuOptions {

    public static final String SCREENSHOT_DIRECTORY = System.getProperty("user.dir") + "\\Screenshots\\Script Screenshots\\";
    public static final String EXPECTED_SCREENSHOT_DIRECTORY = System.getProperty("user.dir") + "\\Screenshots\\WebApp Expected Screenshots\\";
    public static final String DOWNLOAD_DIRECTORY_PATH = System.getProperty("user.dir") + "\\Downloads";
    public WebAppAttributeTable webAppAttributeTable;
    public ScreeningWidgetPopup screeningWidgetPopup;
    public QueryWidgetPopup queryWidgetPopup;

    @FindBy(id = "map")
    WebElement map;

    @FindBy(css = ".if-app.js-open-btn")
    WebElement viewApplicationBtn;

    @FindBy(css = ".if-map.js-open-mapViewer")
    WebElement openInMapViewerBtn;

    @FindBy(css = ".header-section .jimu-title")
    WebElement webAppHeaderTitle;

    @FindBy(css = "div[title='Print']")
    WebElement printWidget;

    @FindBy(css = ".esriSignInDialog .dijitDialogCloseIcon")
    WebElement errorPopupCancelBtn;

    //@FindBy(css = "div.jimu-widget-print input[name='layout']")
    @FindBy(css = "div.jimu-widget-print #dijit_form_Select_0 .dijitArrowButtonInner")
    WebElement printLayoutDrpDwn;

    @FindBy(css = "div.dijitPopup table.dijitMenuActive tr.dijitMenuItem")
    List<WebElement> printWidgetDropDwnOptions;

    @FindBy(css = "div.jimu-widget-print #dijit_form_Select_1 .dijitArrowButtonInner")
    WebElement printFormatDrpDwn;

    @FindBy(css = "div[data-dojo-attach-point='printButtonDijit']")
    WebElement printBtn;

    @FindBy(css = "div[data-dojo-attach-point='advancedButtonDijit']")
    WebElement printWidgetAdvancedBtn;

    @FindBy(id = "dijit_form_RadioButton_0")
    WebElement mapExtentRadioBtn;

    @FindBy(css = ".dijitProgressBarLabel")
    WebElement printProgressBar;

    @FindBy(css = "span[data-dojo-attach-point='successNode']")
    WebElement printJobLink;

    @FindBy(css = "div[data-dojo-attach-point='clearActionBarNode'] .jimu-btn")
    WebElement clearPrintsBtn;

    @FindBy(css = "div[title='Layer List']")
    WebElement layerList;

    @FindBy(css = ".layers-section .layers-list-body>.layer-row")
    List<WebElement> layerListLayers;

    @FindBy(css = ".layers-section .layers-list-body .layer-sub-node>.layer-row")
    List<WebElement> layerListSublayers;

    @FindBy(css = "#map_layers>div")
    List<WebElement> mapLayers;

    @FindBy(css = "div[title='Query']")
    WebElement query;

    @FindBy(css = "div.list-item-name")
    List<WebElement> queryFeaturesList;

    @FindBy(css = "div.btn-execute")
    WebElement queryWidgetApplyBtn;

    @FindBy(css = "div.query-result-action-button")
    WebElement queryResultActionBtn;

    @FindBy(css = "div.no-result-section")
    WebElement noResultInQueryWidgetResultsSection;

    @FindBy(css = "td.field-ACCESS_TRACK_ID")
    WebElement accessTrackIDFieldInAttributeTable;

    @FindBy(css = "td.field-Name")
    WebElement empBoundaryNameFieldInAttributeTable;

    @FindBy(css = "div[title='Legend']")
    WebElement legend;

    @FindBy(css = "div[title='Basemap Gallery']")
    WebElement basemapGallery;

    @FindBy(css = ".esriBasemapGalleryNode")
    List<WebElement> baseMaps;

    @FindBy(css = "div[title='Bookmark']")
    WebElement bookmark;

    @FindBy(css = ".bookmarker-nodes .jimu-img-node") //".webmap .jimu-img-node"
    List<WebElement> bookmarksList;

    @FindBy(css = "div.add")
    WebElement addBookmarkBtn;

    @FindBy(css = "input[title='bookmark']")
    WebElement newBookmarkTitleInput;

    @FindBy(css = "div[title='Draw']")
    WebElement draw;

    @FindBy(css = ".draw-items .draw-item")
    List<WebElement> drawModes;

    @FindBy(css = "g[id*='Draw'] circle, g[id*='Draw'] image, g[id*='Draw'] path, g[id*='Draw'] text")
    List<WebElement> drawnItemsOnMap;

    @FindBy(css = "div[title='Measurement']")
    WebElement measurement;

    @FindBy(css = ".esriMeasurementResultValue .result")
    WebElement measurementResult;

    @FindBy(css = ".esriToggleButton")
    WebElement measurementDropDwn;

    @FindBy(css = "div[title='Add Data']")
    WebElement addData;

    @FindBy(css = "input.search-textbox")
    WebElement addDataSearchBox;

    @FindBy(css = "div.item-card-inner")
    List<WebElement> addDataSearchedLayersList;

    @FindBy(css = ".jimu-widget div[title='Select']")
    WebElement select;

    @FindBy(css = ".jimu-widget-select .tool-section .feature-action")
    WebElement selectWidgetMoreOptions;

    @FindBy(css = "div[title='Make all layers selectable']")
    WebElement makeAllLayersSelectableOpt;

    @FindBy(css = "div[title='Edit']")
    WebElement edit;

    @FindBy(css = ".dojoxGridContent .itemLabel")
    List<WebElement> editNoteOptions;

    @FindBy(css = "div[title='Share']")
    WebElement share;

    @FindBy(css = ".homeContainer")
    WebElement homebutton;

    @FindBy(css = "div.zoom-out")
    WebElement zoomOutBtn;

    @FindBy(css = "div.zoom-in")
    WebElement zoomInBtn;

    @FindBy(css = "input.searchInput")
    WebElement searchPlaceTxtBox;

    @FindBy(css = ".suggestionsMenu li")
    List<WebElement> suggestionsMenu;

    @FindBy(css = ".searchResults .searchMenu li")
    List<WebElement> searchMenu;

    @FindBy(css = "div.searchLoading")
    WebElement searchLoading;

    @FindBy(css = ".esriPopupVisible .esriViewPopup")
    WebElement searchPopup;

    @FindBy(css = ".esriPopupVisible .close")
    WebElement searchPopupCloseBtn;

    @FindBy(css = ".searchBtn.searchToggle")
    WebElement searchInDrpDwnBtn;

    @FindBy(css = ".searchMenu.sourcesMenu li")
    List<WebElement> searchInOptionsList;

    @FindBy(css = ".jimu-popup-action-btn")
    WebElement errorPopupOkBtn;

    @FindBy(css = ".enable-btn[title='OK'")
    WebElement splashScreenOkBtn;

    @FindBy(css = "g[id*='ENV_CH_Map_Notes'] circle")
    WebElement env_Map_Notes;

    @FindBy(css = ".esriPopup .mainSection .header")
    WebElement mapPopup;

    @FindBy(css = ".sizer .popup-menu-button")
    WebElement mapFeaturePopupMoreOptionsLink;

    @FindBy(css = ".popup-menu-item div[title='View in Attribute Table']")
    WebElement viewInAttributeTableOption;

    @FindBy(css = ".jimu-dropmenu .feature-action")
    WebElement layerPanelFeatureActionLink;

    @FindBy(css = "div[itemid='turnAllLayersOn']")
    WebElement turnOnAllLayersOption;

    @FindBy(css = "div[itemid='turnAllLayersOff']")
    WebElement turnOffAllLayersOption;

    @FindBy(css = ".symbol-set-table input[aria-valuenow='24']")
    WebElement symbolSizeInput;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointColorTr'] .jimu-color-picker")
    WebElement colorPicker;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointOutlineColorTr'] .jimu-color-picker")
    WebElement outlineColorPicker;

    @FindBy(css = ".dijitTooltipDialogPopup:not([style*='display']) .jimu-color-selector input[id*='_r']")
    WebElement color_r;

    @FindBy(css = ".dijitTooltipDialogPopup:not([style*='display']) .jimu-color-selector input[id*='_g']")
    WebElement color_g;

    @FindBy(css = ".dijitTooltipDialogPopup:not([style*='display']) .jimu-color-selector input[id*='_b']")
    WebElement color_b;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointOpacityTr'] .dijitSliderImageHandle")
    WebElement transparencySlider;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointOpacityTr'] .dijitSliderDecrementIconH")
    WebElement decrementTransparency;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointOpacityTr'] .dijitSliderIncrementIconH")
    WebElement incrementTransparency;

    @FindBy(css = ".point-section tr[data-dojo-attach-point='pointOulineWidthTr'] input[data-dojo-attach-point*='textbox']")
    WebElement outlineWidth;

    @FindBy(css = "div[aria-label='Undo']")
    WebElement undoBtn;

    @FindBy(css = "div[aria-label='Redo']")
    WebElement redoBtn;

    @FindBy(css = "div[aria-label='Clear']")
    WebElement clearBtn;

    @FindBy(css = ".text-section .text-input")
    WebElement textInputForTxtNote;

    @FindBy(css = ".text-section .text-symbol-section .jimu-color-picker")
    WebElement textColorPicker;

    @FindBy(css = ".text-section .text-symbol-section .dijitInputField input[type='text']")
    WebElement fontSizeTxtBox;

    @FindBy(css = "table.esriLegendLayer td[align='left']")
    List<WebElement> legendItems;

    @FindBy(css = "table.esriLegendLayer td[align='center'] path, table.esriLegendLayer td[align='center'] img") //path
    List<WebElement> legendIcons;

    @FindBy(css = ".btn-select")
    WebElement selectBtn;

    @FindBy(css = "div.selectable-layer-item")
    List<WebElement> selectWidgetLayers;

    @FindBy(css = ".save-button span.dijitButtonNode")
    WebElement pointNoteSaveBtn;

    @FindBy(css = ".close-button span.dijitButtonNode")
    WebElement pointNoteCloseBtn;

    @FindBy(css = ".atiDeleteButton span.dijitButtonNode")
    WebElement pointNoteDeleteBtn;

    @FindBy(css = "g[data-geometry-type='point'][id*='Map_Notes']")
    WebElement pointMapNote;

    @FindBy(xpath = "//div[@class='atiAttributes']//td[@data-fieldname='title']/..//div[contains(@class,'dijitInputContainer')]/input")
    WebElement pointNoteTitleTxtBox;

    @FindBy(xpath = "//div[@class='atiAttributes']//td[@data-fieldname='description']/..//div[contains(@class,'dijitInputContainer')]/input")
    WebElement pointNoteDescriptionTxtBox;

    @FindBy(css = "div.coordinate-info")
    WebElement coordinateInfo;

    @FindBy(css = "g#graphicsLayer1_layer image")
    WebElement esriGreenPin;

    @FindBy(css = "div.coordinate-locate-container")
    private WebElement enableCoordinateLocateBtn;

    @FindBy(css = ".jimu-popup .title-label")
    WebElement webAppPopup;

    @FindBy(css = ".jimu-popup-action-btn")
    WebElement webAppPopupOkBtn;

    @FindBy(css = ".jimu-input")
    WebElement webappCreateLayerNameInput;

    @FindBy(css = ".jimu-popup-action-btn[title='OK']")
    WebElement createOrSaveLayerPopupOkBtn;

    @FindBy(css = ".jimu-popup input[id *= 'TextBox']")
    WebElement saveToMyContentTitleTxtBox;

    @FindBy(css = "div[title='Network Report']")
    WebElement screeningWidget;

    @FindBy(css = "div[title='Query']")
    WebElement queryWidget;

    @FindBy(css = "div[title='Edit']")
    WebElement editWidget;

    public WebAppPage() {
        PageFactory.initElements(driver, this);
        webAppAttributeTable = new WebAppAttributeTable(driver);
        screeningWidgetPopup = new ScreeningWidgetPopup(driver);
        queryWidgetPopup = new QueryWidgetPopup(driver);
    }

    public void clickOnPopupOkBtn(){
        if(errorPopupOkBtn.isDisplayed()){
            errorPopupOkBtn.click();
            sleep(500);
        }
    }

    public void clickOnSplashScreenOkBtn(){
        splashScreenOkBtn.click();
        sleep(500);
    }

    @Step("Click on View Application")
    public void clickOnViewApplication() {
        viewApplicationBtn.click();
    }

    @Step("Click on Open in Map Viewer")
    public void clickOnOpenInMapViewer() {
        openInMapViewerBtn.click();
    }

    @Step("Verify that web application is opened in a new window")
    public void verifyThatWebAppIsOpenedInNewWindow() {
        assertThat(driver.getWindowHandles().size()).isEqualTo(2);
        switchToNewWindow();
        sleep(1000);
    }

    @Step("Verify the title of the web application is {webAppTitle}")
    public void verifyTheTitleOfWebApp(String webAppTitle) {
        sleep(2000);
        wait.until(ExpectedConditions.visibilityOf(webAppHeaderTitle));
        assertThat(webAppHeaderTitle.getText().trim()).isEqualToIgnoringCase(webAppTitle);
        sleep(1000);
        //errorPopupCancelBtn.click();
        //clickOnPopupOkBtn();
    }

    @Step("Click on Legend")
    public void clickOnLegendTab(){
        legend.click();
        sleep(500);
    }

    @Step("Click on Query Widget")
    public void clickOnQueryTab(){
        query.click();
        sleep(3000);
    }

    @Step("Select feature {featureName} from the list")
    public void clickOnQueryFeature(String featureName) {
        queryFeaturesList.stream().filter(feature -> feature.getText().equalsIgnoreCase(featureName)).findFirst().get().click();
    }

    @Step("Input values for Access Track Id as {accessTrackId} and Track Name as {trackName}")
    public void inputValuesInQueryCriteriaForAccessTrack(String accessTrackId, String trackName) {
        List<WebElement> headerList = driver.findElements(By.cssSelector(".params-container .second-tr"));
        headerList.stream().filter(header -> header.getText().contains("EMP Boundary Name")).findFirst().get(). // Access Track ID
                findElement(By.cssSelector("input[data-dojo-attach-point]")).sendKeys(accessTrackId);
        /*headerList.stream().filter(header -> header.getText().contains("TRACK_NAME")).findFirst().get().
                findElement(By.cssSelector("input[data-dojo-attach-point]")).sendKeys(trackName);*/
    }

    @Step("Click on Apply button")
    public void clickOnQueryWidgetApplyBtn() {
        queryWidgetApplyBtn.click();
        sleep(1000);
    }

    @Step("Click on more options button on Query Results pane")
    public void clickOnQueryResultActionBtn() {
        queryResultActionBtn.click();
        sleep(5000);
    }

    @Step("Verify that Results section is displayed with features attribute")
    public void verifyThatResultsSectionIsDisplayed() {
        assertThat(noResultInQueryWidgetResultsSection.isDisplayed()).isFalse();
    }

    @Step("Verify that Results are removed")
    public void verifyThatResultsAreRemovedFromResultSection() {
        assertThat(noResultInQueryWidgetResultsSection.isDisplayed()).isTrue();
    }

    @Step("Click on {action} option from the dropdown")
    public void selectQueryResultAction(String action) {
        sleep(2000);
        WebElement actionElem = driver.findElement(By.cssSelector("div[title='"+action+"']"));
        wait.until(ExpectedConditions.elementToBeClickable(actionElem));
        actionElem.click();
    }

    @Step("Verify that the selected feature with Access Track ID {expectedFeatureValue} is displayed in the attribute table")
    public void verifyThatQueryFilterIsAppliedInAttributeTable(String expectedFeatureValue) {
        assertThat(accessTrackIDFieldInAttributeTable.getText()).containsIgnoringCase(expectedFeatureValue);
    }

    @Step("Verify that the selected feature with EMp Boundary Name {expectedFeatureValue} is displayed in the attribute table")
    public void verifyThatQueryFilterIsAppliedInAttributeTable2(String expectedFeatureValue) {
        assertThat(empBoundaryNameFieldInAttributeTable.getText()).containsIgnoringCase(expectedFeatureValue);
    }

    @Step("Click on Layer List")
    public void clickOnLayerListTab(){
        sleep(5000);
        layerList.click();
        sleep(3000);
    }

    @Step("Click on Print button to open the Print widget")
    public void clickOnPrintWidgetTab(){
        sleep(5000);
        printWidget.click();
        sleep(3000);
    }

    @Step("Click on Print Layout drop down")
    public void clickOnPrintLayoutDrpDwn() {
        action.moveToElement(printLayoutDrpDwn).click().perform();
    }

    @Step("Click on Print Format drop down")
    public void clickOnPrintFormatDrpDwn() {
        //printFormatDrpDwn.click();
        action.moveToElement(printFormatDrpDwn).click().perform();
    }

    @Step("Select print option as {optionToBeSelected}")
    public void selectPrintDropDownOption(String optionToBeSelected) {
        action.click(printWidgetDropDwnOptions.stream().filter(option -> option.getAttribute("aria-label").contains(optionToBeSelected)).findFirst().get()).
                pause(1).
                build().
                perform();
    }

    @Step("Click on Print button")
    public void clickOnPrintBtn() {
        sleep(5000);
        printBtn.click();
    }

    @Step("Click on Advanced button")
    public void clickOnAdvancedBtn() {
        printWidgetAdvancedBtn.click();
    }

    @Step("In advanced options select map extent")
    public void selectPreserveMapExtent() {
        mapExtentRadioBtn.click();
    }

    @Step("Verify that progress bar displays next to the executing task")
    public void verifyThatPrintProgressBarIsDisplayed() {
        assertThat(printProgressBar.isDisplayed()).isTrue();
    }

    @Step("Verify that link to the print job is displayed")
    public void verifyThatLinkForThePrintJobIsDisplayed() {
        sleep(5000);
        wait.until(ExpectedConditions.visibilityOf(printJobLink));
        assertThat(printJobLink.isDisplayed()).isTrue();
    }

    @Step("Verify that link to the print job is NOT displayed")
    public void verifyThatLinkForThePrintJobIsNotDisplayed() {
        try{
            assertThat(printJobLink.isDisplayed()).isFalse();
        }
        catch (Exception e){
        }
    }

    @Step("Click the task to open the file in a new PDF window")
    public void clickOnPrintJobLink() {
        printJobLink.click();
    }

    public Set<String> getCurrentWindowHandles() {
        return driver.getWindowHandles();
    }

    @Step("Verify that PDF file is opened in a new window")
    public void verifyThatPDFFileIsOpenedInNewWindow(){
        Set<String> currentWindowHandles = driver.getWindowHandles();
        assertThat(currentWindowHandles.size()).isEqualTo(3);
        switchToNewWindow();
        assertThat(driver.getCurrentUrl().contains("pdf")).isTrue();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
            if(driver.getCurrentUrl().contains("webappviewer"))
                break;
        }
    }

    @Step("Click on Clear prints button")
    public void clickOnClearPrints(){
        clearPrintsBtn.click();
    }

    @Step("Click on Basemap Gallery")
    public void clickOnBasemapGalleryTab(){
        basemapGallery.click();
    }

    @Step("Select a base map {baseMapToSelect}")
    public void selectBaseMap(String baseMapToSelect) {
        baseMaps.stream().filter(map -> map.getText().equalsIgnoreCase(baseMapToSelect)).collect(Collectors.toList()).get(0).click();
        sleep(3000);
    }

    @Step("Close panel for widget {widget}")
    public void closePanelForWidget(String widget) {
        driver.findElement(By.cssSelector(".jimu-panel-title div[title='"+widget+"'] + .btns-container .close-btn")).click();
    }

    @Step("Click on a feature on the map")
    public void clickOnAFeatureForEnvRestrictedMap(){
        action.moveToElement(env_Map_Notes).
                click().build().
                perform();
        sleep(2000);
    }

    public void clickOnFeatureActionInLayersPanel(){
        layerPanelFeatureActionLink.click();
    }

    public void turnOnLayer(String layerName){
        driver.findElement(By.cssSelector("tr[aria-label*='"+layerName+"'] .jimu-checkbox")).click();
    }

    public void turnOnAllLayersOfTheWebApp(){
        clickOnFeatureActionInLayersPanel();
        //turnOnAllLayersOption.click();
        action.moveToElement(turnOnAllLayersOption).click().perform();
        sleep(10000);
    }

    public void turnOffAllLayersOfTheWebApp(){
        clickOnFeatureActionInLayersPanel();
        turnOffAllLayersOption.click();
        sleep(1000);
    }

    @Step("Set symbol size to {newSize}")
    public void setSymbolSizeAs(String newSize){
        symbolSizeInput.clear();
        symbolSizeInput.sendKeys(newSize);
    }

    public void changeOutlineColorOfTheSymbolTo(String r, String g, String b) {
        outlineColorPicker.click();
        setColorTo(r, g, b);
    }

    public void changeColorOfTheSymbolTo(String r, String g, String b) {
        colorPicker.click();
        setColorTo(r, g, b);
        colorPicker.click();
    }

    public void setTransparency(int transparencyValue) {
        String valueNow = transparencySlider.getAttribute("aria-valuenow");
        if(Integer.parseInt(valueNow) > transparencyValue && Integer.parseInt(valueNow) != transparencyValue) {
          while (Integer.parseInt(valueNow) > transparencyValue) {
              decrementTransparency.click();
              valueNow = transparencySlider.getAttribute("aria-valuenow");
          }
        }
        else {
            while (Integer.parseInt(valueNow) < transparencyValue) {
                incrementTransparency.click();
                valueNow = transparencySlider.getAttribute("aria-valuenow");
            }
        }
    }

    @Step("Set outline width as {width}")
    public void setOutlineWidth(String width) {
        outlineWidth.clear();
        outlineWidth.sendKeys(width);
        outlineWidth.sendKeys(Keys.ENTER);
    }

    @Step("Click on Undo Button")
    public void clickOnUndoBtn() {
        undoBtn.click();
    }

    @Step("Click on Redo Button")
    public void clickOnRedoBtn() {
        redoBtn.click();
    }

    @Step("Click on Clear Button")
    public void clickOnClearBtn() {
        clearBtn.click();
    }

    @Step("Input text for a text note")
    public void inputTextForTextNote(String text) {
        textInputForTxtNote.sendKeys(text);
    }

    @Step("Change color of the text")
    public void changeColorOfTheTextTo(String r, String g, String b) {
        textColorPicker.click();
        setColorTo(r, g, b);
    }

    public void setColorTo(String r, String g, String b) {
        color_r.clear();
        color_r.sendKeys(r);
        color_r.sendKeys(Keys.ENTER);
        color_g.clear();
        color_g.sendKeys(g);
        color_g.sendKeys(Keys.ENTER);
        color_b.clear();
        color_b.sendKeys(b);
        color_b.sendKeys(Keys.ENTER);
    }

    @Step("Change font size of the text to {newSize}")
    public void setFontSizeOfTheText(String newSize) {
        fontSizeTxtBox.clear();
        fontSizeTxtBox.sendKeys(newSize);
        fontSizeTxtBox.sendKeys(Keys.ENTER);
    }

    @Step("Click on a feature on the map")
    public void clickOnAFeatureForEnvRestrictedMap2(){
        action.moveToElement(map).moveByOffset(10,10).
                click().build().
                perform();
        sleep(2000);
    }

    @Step("Verify that Popup is displayed on the map")
    public void verifyThatPopupIsPresentOnMap(){
        assertThat(mapPopup.isDisplayed()).isTrue();
    }

    @Step("Click on More options in the popup displayed for the feature")
    public void clickOnMoreOptionsBtnInPopup() {
        mapFeaturePopupMoreOptionsLink.click();
    }

    @Step("Click on View In Attribute Table option")
    public void clickOnViewInAttributeTable() {
        viewInAttributeTableOption.click();
    }

    public void verifyThatAttributeTableIsDisplayed() {
        sleep(500);
        assertThat(driver.findElement(By.cssSelector("div.jimu-widget-attributetable")).getAttribute("class")).doesNotContain("closed");
    }

    @Step("Click on Bookmark")
    public void clickOnBookmarkTab(){
        bookmark.click();
    }

    @Step("Select bookmark {bookmarkToSelect}")
    public void selectBookMark(String bookmarkToSelect) {
        getBookMark(bookmarkToSelect).click();
        sleep(2000);
    }

    public WebElement getBookMark(String bookmarkToSelect) {
        return bookmarksList.stream().filter(bookmark -> bookmark.getAttribute("aria-label").equalsIgnoreCase(bookmarkToSelect)).findFirst().get();
    }

    @Step("Click on add bookmark button")
    public void clickOnAddBookmarkBtn() {
        addBookmarkBtn.click();
        sleep(2000);
    }

    @Step("Input bookmark name and save the bookmark")
    public void inputBookmarkNameAs(String name) {
        newBookmarkTitleInput.clear();
        newBookmarkTitleInput.sendKeys(name);
        action.moveToElement(newBookmarkTitleInput, 100, 100);
    }

    public List<String> getListOfBookmarks(){
        return bookmarksList.stream().map(bookmark -> bookmark.getAttribute("aria-label")).collect(Collectors.toList());
    }

    @Step("Verify the bookmarks {expectedBookmarks} are present")
    public void verifyTheBookmarksArePresent(List<String> expectedBookmarks) {
        final List<String> listOfBookmarks = getListOfBookmarks();
        System.out.println(listOfBookmarks);
        assertThat(listOfBookmarks.size()).isEqualTo(expectedBookmarks.size());
        assertThat(listOfBookmarks).containsExactlyElementsOf(expectedBookmarks);
    }

    @Step("Delete the bookmark {bookmarkName}")
    public void deleteBookMark(String bookmarkName) {
        getBookMark(bookmarkName).findElement(By.cssSelector("div[title='Delete the bookmark']")).click();
        sleep(2000);
    }

    @Step("Click on Draw")
    public void clickOnDrawTab(){
        sleep(500);
        draw.click();
    }

    public void selectDrawMode(String modeToSelect) {
        drawModes.stream().filter(item -> item.getAttribute("title").equalsIgnoreCase(modeToSelect)).findFirst().get().click();
    }

    public int getNoOfItemsDrawnOnMap() {
        return drawnItemsOnMap.size();
    }

    @Step("Verify that item is added on the map")
    public void verifyThatItemIsAddedOnTheMap() {
        assertThat(getNoOfItemsDrawnOnMap()).isEqualTo(1);
    }

    @Step("Verify that item is removed from the map")
    public void verifyThatItemIsRemovedFromTheMap() {
        assertThat(getNoOfItemsDrawnOnMap()).isEqualTo(0);
    }

    @Step("Verify that the color of the feature is same as the chosen color")
    public void verifyTheColorOfTheItem(String rgbValue) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getAttribute("fill")).contains(rgbValue);
    }

    @Step("Verify that the outline color of the feature is same as the chosen color")
    public void verifyTheOutlineColorOfTheItem(String rgbValue) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getAttribute("stroke")).contains(rgbValue);
    }

    @Step("Verify that the outline width of the feature is same as expected")
    public void verifyTheOutlineWidthOfTheItem(String expectedWidth) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getAttribute("stroke-width")).isEqualTo(expectedWidth);
    }

    @Step("Verify that the size of the symbol is same as the selected size")
    public void verifyTheSizeOfPointNote(String selectedSize) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getAttribute("r")).isEqualTo(String.valueOf(Integer.parseInt(selectedSize)/2));
    }

    @Step("Verify that font size of the text note is same as the selected size")
    public void verifyTheSizeOfTextNote(String selectedSize) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getAttribute("font-size")).isEqualTo(selectedSize);
    }

    @Step("Verify that text note {expectedText} is displayed on the map")
    public void verifyTextOfTheTextNote(String expectedText) {
        assertThat(drawnItemsOnMap.stream().findFirst().get().getText()).isEqualTo(expectedText);
    }

    @Step("Click on Measurement")
    public void clickOnMeasurementTab(){
        measurement.click();
    }

    @Step("Select {measuringType} to measure")
    public void selectTypeOfMeasuringToPerform(String measuringType){
        action.moveToElement(driver.findElement(By.cssSelector("span[title='"+measuringType+"']"))).click().perform();
        sleep(2000);
    }

    @Step("Draw area on the map to measure")
    public void drawAreaToMeasureDistance(){
        action.moveToElement(map,-100, 100).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(0, 100).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(50, 0).click().
                pause(Duration.ofSeconds(1)).
                doubleClick().
                perform();
        //action.moveByOffset(-50, -50).click().perform();
        //action.moveByOffset(-100, -100).doubleClick().perform();
        sleep(2000);
    }

    @Step("Draw a point on map")
    public void drawPointOnTheMap(){
        //action.moveByOffset(-50, -50).click().perform();
        action.moveToElement(map,-100, 100).click().perform();
        sleep(2000);
    }

    @Step("Verify that latitude and longitude of the point is displayed")
    public void verifyLatitudeAndLongitudeOfThePoint(){
        String latitude = driver.findElement(By.cssSelector(".esriMeasurementTableRow [dojoattachpoint='markerLatitude']")).getText();
        String longitude = driver.findElement(By.cssSelector(".esriMeasurementTableRow [dojoattachpoint='markerLatitude']")).getText();
        System.out.println(latitude +" -- "+ longitude);
        assertThat(latitude).isNotBlank();
        assertThat(longitude).isNotBlank();
    }

    @Step("Verify the change in measurement value when unit changes")
    public void verifyMeasurementKmToM(String valueKm, String valueM){
        assertThat(Double.parseDouble(valueKm)).isEqualTo(round((Double.parseDouble(valueM)/1000), 1));

    }

    @Step("Verify the change in measurement value when unit changes")
    public void verifyMeasurementSqKmToM(String valueSqKm, String valueSqM){
        assertThat(Double.parseDouble(valueSqKm)).isEqualTo(round((Double.parseDouble(valueSqM)/1000000), 1));
    }

    @Step("Get measurement value")
    public String getMeasurementResultValue(){
        System.out.println(measurementResult.getText());
        return measurementResult.getText();
    }

    @Step("Click on measurement unit dropdown")
    public void clickOnMeasurementDropDwn(){
        measurementDropDwn.click();
    }

    @Step("Select measurement unit {unit}")
    public void selectMeasurementUnit(String unit){
        driver.findElement(By.cssSelector(".unitDropDown[aria-label='"+unit+" ']")).click();
    }

    @Step("Draw a line to measure the distance")
    public void drawLineToMeasureDistance(){
        /*action.moveByOffset(-50, -50).click().perform();
        action.moveByOffset(-100, -100).doubleClick().perform();*/
        action.moveToElement(map,-150, 105).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(0, 80).doubleClick().
                perform();
        sleep(2000);
    }

    @Step("Click on Add Data")
    public void clickOnAddDataTab(){
        addData.click();
    }

    @Step("Search a layer {layerName} to add to the map")
    public void searchLayerToAddToTheWebApp(String layerName) {
        addDataSearchBox.clear();
        addDataSearchBox.sendKeys(layerName);
        addDataSearchBox.sendKeys(Keys.ENTER);
        sleep(3000);
    }

    @Step("Add layer {layerName} to the map")
    public void addLayerToWebApp(String layerName, String typeOfLayer) {
        addDataSearchedLayersList.stream().filter(layer -> layer.getText().contains(layerName) && layer.getText().contains(typeOfLayer))
                .findFirst().get()
                .findElement(By.cssSelector("a[data-dojo-attach-point='addButton']")).click();
    }

    @Step("Click on Select")
    public void clickOnSelectTab(){
        //select.click();
        clickOnElementWithAdvancedJS(select);
        wait.until(ExpectedConditions.elementToBeClickable(selectBtn));
        //sleep(2000);
    }

    @Step("Click on More Options at the top in Select widget")
    public void clickOnMoreOptionsInSelectWidget() {
        selectWidgetMoreOptions.click();
    }

    public void clickOnMakeAllLayersSelectable() {
        clickOnMoreOptionsInSelectWidget();
        makeAllLayersSelectableOpt.click();
        sleep(1000);
    }

    @Step("Click on Select button")
    public void clickOnSelectBtn(){
        sleep(1000);
        selectBtn.click();
        sleep(2000);
    }

    public List<WebElement> getSelectWidgetLayersWithCountGreaterThanZero(){
        return selectWidgetLayers.stream().filter(layer -> Integer.parseInt(layer.findElement(By.cssSelector(".selected-num")).getText().trim())>0).collect(Collectors.toList());
    }

    public String getSelectWidgetLayerWithHighestCount() {
        List<WebElement> list = getSelectWidgetLayersWithCountGreaterThanZero();
        if(list.size()>0) {
            List<WebElement> sortedList = getSelectWidgetLayersWithCountGreaterThanZero().stream().sorted(Comparator.comparing(item -> Integer.parseInt(item.findElement(By.cssSelector(".selected-num")).getText()))).collect(Collectors.toList());
            return sortedList.get(sortedList.size() - 1).findElement(By.cssSelector(".layer-name")).getText();
        }
        return null;
    }

    public int getTheCountForLayerInSelectWidget(String layerName) {
       return Integer.parseInt(getLayerInSelectWidget(layerName).findElement(By.cssSelector(".selected-num")).getText().trim());
    }

    public WebElement getLayerInSelectWidget(String layerName) {
        return selectWidgetLayers.stream().filter(layer -> layer.getText().contains(layerName)).findFirst().get();
    }

    public void clickOnMoreOptionsForLayerInSelectWidget(String layerName) {
        getLayerInSelectWidget(layerName).findElement(By.cssSelector(".feature-action")).click();
    }

    @Step("Click on more options for a layer for which the count is greater than zero")
    public int clickOnMoreOptionsForLayerWithCountGreaterThanZero() {
        List<WebElement> list = getSelectWidgetLayersWithCountGreaterThanZero();
        List<WebElement> sortedList = list.stream().sorted(Comparator.comparing(item -> Integer.parseInt(item.findElement(By.cssSelector(".selected-num")).getText()))).collect(Collectors.toList());

        int count = 0;
        if(sortedList.size()>0){
            String layer = sortedList.get(sortedList.size()-1).findElement(By.cssSelector(".layer-name")).getText().trim();
            System.out.println("Selected layer is : " + layer);
            count = Integer.parseInt(list.get(sortedList.size()-1).findElement(By.cssSelector(".selected-num")).getText().trim());
            list.get(sortedList.size()-1).findElement(By.cssSelector(".feature-action")).click();
            sleep(500);
        }
        return count;
    }

    @Step("Select {optionToSelect} from the menu")
    public void selectOptionToExportInSelectWidget(String optionToSelect) {
        driver.findElement(By.cssSelector("div.popup-menu div[title*='"+optionToSelect+"']")).click();
        sleep(5000);
    }

    @Step("Verify that the no of features displayed for the layer in the selct widget is same as the no of features in the downloaded file")
    public void verifyTheCount(int expectedCount, int actualCount) {
        assertThat(expectedCount).isEqualTo(actualCount);
    }

    public int getNoOfRecordsInCSVFile(String fileName) {
        try {
            //System.out.println("The count is : " + getCountOfRecordsInTheLatestDownloadedFile(DOWNLOAD_DIRECTORY_PATH + "\\" + fileName));
            return getCountOfRecordsInTheLatestDownloadedFile(DOWNLOAD_DIRECTORY_PATH + "\\" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Step("Draw an area on the map")
    public void drawAnAreaOnTheMap(int start_x, int start_y, int end_x, int end_y){
        action.moveToElement(map, end_x, end_y).clickAndHold().pause(1).moveToElement(map, start_x, start_y).release(map).pause(1).perform();
        sleep(8000);
    }

    @Step("Click on Edit")
    public void clickOnEditTab(){
        edit.click();
    }

    @Step("Select {} Notes from editable features list")
    public void selectEditNoteOption(String optionToSelect) {
        editNoteOptions.stream().filter(option -> option.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
    }

    @Step("Click on Share")
    public void clickOnShareTab(){
        share.click();
    }

    public void clickOnZoomIn(){
        zoomInBtn.click();
    }

    @Step("Click on Zoom In")
    public void clickOnZoomIn(int number){
        while (number>0){
            zoomInBtn.click();
            number--;
        }
        sleep(5000);
    }

    public void clickOnZoomOut(){
        zoomOutBtn.click();
    }

    @Step("Click on Home button")
    public void clickOnHomeBtn(){
        homebutton.click();
    }

    public void clickOnZoomOut(int number){
        while (number>0){
            zoomOutBtn.click();
            number--;
        }

    }

    public void inputPlaceOrAddressToFindOnMap(String placeToBeSearched) {
        boolean flag = false;
        searchPlaceTxtBox.clear();
        searchPlaceTxtBox.sendKeys(placeToBeSearched);
        sleep(2000);
    }

    @Step("Search for - {searchString} on the map")
    public void searchOnMap(String searchString) {
        inputPlaceOrAddressToFindOnMap(searchString);
        sleep(1000);
        suggestionsMenu.stream().forEach(option -> System.out.println(option.getText()));
        suggestionsMenu.stream().filter(menu -> menu.getText().toLowerCase().contains(searchString.toLowerCase())).findFirst().get().click();
        sleep(1000);
    }

    @Step("Search for - {searchString} on the map")
    public void searchOnMap2(String searchString) {
        inputPlaceOrAddressToFindOnMap(searchString);
        searchPlaceTxtBox.sendKeys(Keys.ENTER);
        sleep(5000);
        //wait.until(ExpectedConditions.visibilityOfAllElements(searchMenu));
        //wait.until(ExpectedConditions.invisibilityOf(searchLoading));
        searchMenu.stream().forEach(menu-> System.out.println(menu.getText()));
        searchMenu.stream().filter(menu -> menu.getText().toLowerCase().contains(searchString.toLowerCase())).findFirst().get().click();
        sleep(8000);
    }

    @Step("Verify that searched address popup is displayed on the map")
    public void verifyThatAddressPopupIsDisplayed(){
        assertThat(searchPopup.findElement(By.cssSelector(".mainSection .header")).isDisplayed()).isTrue();
    }

    @Step("Close the popup")
    public void closeSearchPopup(){
        searchPopupCloseBtn.click();
    }

    @Step("Click on Search In dropdown")
    public void clickOnSearchInDrpDwn(){
        searchPlaceTxtBox.clear();
        sleep(1000);
        searchInDrpDwnBtn.click();
    }

    public List<String> getListOfSearchInOptions(){
        return searchInOptionsList.stream().map(option -> option.getText()).collect(Collectors.toList());
    }

    @Step("Verify the options available to search in are {expectedSearchOptions}")
    public void verifyTheBuiltInSearchOptions(List<String> expectedSearchOptions) {
        final List<String> listOfSearchInOptions = getListOfSearchInOptions();
        System.out.println(listOfSearchInOptions);
        assertThat(listOfSearchInOptions.size()).isEqualTo(expectedSearchOptions.size());
        assertThat(listOfSearchInOptions).containsExactlyInAnyOrderElementsOf(expectedSearchOptions);
    }

    @Step("Select search in option {optionToSelect}")
    public void selectSearchInOption(String optionToSelect) {
        searchInOptionsList.stream().filter(option -> option.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
        sleep(1000);
    }

    public boolean isLayerDisplayedInContentPane(String layerName) {
        return layerListLayers.stream().filter(layer -> layer.getAttribute("aria-label").contains(layerName)).collect(Collectors.toList()).size() >= 1;
    }

    public boolean isSubLayerDisplayedInContentPane(String layerName) {
        return layerListSublayers.stream().filter(layer -> layer.getAttribute("aria-label").contains(layerName)).collect(Collectors.toList()).size() >= 1;
    }

    public boolean isLayerDisplayedOnMap(String layerName) {
        try{
            WebElement searchedLayer = mapLayers.stream().filter(layer -> layer.getAttribute("id").contains(layerName)).findFirst().get();
            System.out.println(searchedLayer.getAttribute("id"));
            System.out.println(searchedLayer.getCssValue("display"));
            System.out.println(searchedLayer.getCssValue("overflow"));
            if(searchedLayer.getCssValue("display").equalsIgnoreCase("block")
                    && searchedLayer.getCssValue("overflow").equalsIgnoreCase("visible"))
                return true;
            else return false;
        }
        catch (Exception e){
            return false;
        }

    }

    @Step("Verify that layer {layerName} is displayed on the map")
    public void verifyThatLayerIsDisplayedOnTheMap(String layerName) {
        assertThat(isLayerDisplayedOnMap(layerName)).isTrue();
    }

    @Step("Verify that layer {layerName} is displayed on the widget pane")
    public void verifyThatLayerIsDisplayedOnTheWidgetPane(String layerName) {
        assertThat(isLayerDisplayedInContentPane(layerName)).isTrue();
    }

    @Step("Verify that sub layer {layerName} is displayed on the widget pane")
    public void verifyThatSubLayerIsDisplayedOnTheWidgetPane(String layerName) {
        assertThat(isSubLayerDisplayedInContentPane(layerName)).isTrue();
    }

    public WebElement getLayer(String layerName) {
        return driver.findElement(By.cssSelector(".layers-list-body .layer-row[aria-label*='"+layerName+"']"));
    }

    public WebElement getSubLayer(String layerName) {
        return driver.findElement(By.cssSelector(".layers-list-body .layer-sub-node .layer-row[aria-label='Layer "+layerName+"']"));
    }

    @Step("Expand layer {layerName}")
    public void expandLayer(String layerName) {
        getLayer(layerName).findElement(By.cssSelector("div[aria-label='Expand']")).click();
        sleep(1000);
    }

    @Step("Expand layer {layerName}")
    public void expandSubLayer(String layerName) {
        getSubLayer(layerName).findElement(By.cssSelector("div[aria-label='Expand']")).click();
        sleep(1000);
    }

    @Step("Click on More Options for a layer {layerName}")
    public void clickOnMoreOptionsForLayer(String layerName) {
        getLayer(layerName).findElement(By.cssSelector(".layers-list-popupMenu-div ")).click();
    }

    @Step("Click on set visibility range")
    public void clickOnSetVisibilityRange(){
        driver.findElement(visibilityRange).click();
    }

    @Step("Click on Zoom To in more options")
    public void clickOnZoomToForLayer(){
        driver.findElement(zoomTo).click();
        sleep(2000);
    }

    @Step("Click on View In Attribute Table in more options")
    public void clickOnViewInAttributeTableForLayer(){
        driver.findElement(viewInAttributeTable).click();
        sleep(2000);
    }

    public void clickOnVisibilityRangeDrpDwn(){
        driver.findElement(visibilityDrpDwn).click();
    }

    @Step("Select visibility {option}")
    public void selectVisibilityOption(String option) {
        this.clickOnVisibilityRangeDrpDwn();
        WebElement e = driver.findElement(By.cssSelector(".esriScaleMenuPopup .dijitTextBox input[aria-label]"));
        action.moveToElement(e, 10, 10).click().perform();
        driver.findElements(visibilityDrpDwnOpts).stream().filter(opt -> opt.getText().contains(option)).findFirst().get().click();
        sleep(2000);
        //action.moveByOffset(10,10).click().perform();
    }

    @Step("Verify that layer is NOT visible")
    public void verifyVisibilityIsOutOfRangeForLayer(String layerName){
        sleep(2000);
        WebElement layer = getLayer(layerName).findElement(By.cssSelector("div[class*='layer-title']"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute("class").contains("grayed-title")).isTrue();
    }

    @Step("Verify that layer is visible")
    public void verifyVisibilityIsInRangeForLayer(String layerName){
        sleep(4000);
        WebElement layer = getLayer(layerName).findElement(By.cssSelector("div[class*='layer-title']"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute("class").contains("grayed-title")).isFalse();
    }

    public void takeScreenshot(String imageName) {
        sleep(3000);
        Screenshot s = new AShot().
                shootingStrategy(ShootingStrategies.viewportPasting(100)).coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, map);
        try {
            boolean flag = ImageIO.write(s.getImage(), "png", new File(SCREENSHOT_DIRECTORY + imageName + ".png"));
            System.out.println(flag);
            if(imageName.equalsIgnoreCase("Bookmark Expected"))
                FileUtils.copyFileToDirectory(new File(SCREENSHOT_DIRECTORY + imageName + ".png"), new File(EXPECTED_SCREENSHOT_DIRECTORY));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanScreenshotDirectory() {
        try {
            FileUtils.cleanDirectory(new File(SCREENSHOT_DIRECTORY));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   /* public void cleanDownloadDirectory() {
        try {
            FileUtils.cleanDirectory(new File(DOWNLOAD_DIRECTORY_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    @Step("Verify that {expectedImageName} and {actualImageName} are the same")
    public void compareImage(String expectedImageName, String actualImageName) {
        try {
            BufferedImage expectedImage = ImageIO.read(new File(EXPECTED_SCREENSHOT_DIRECTORY + expectedImageName + ".png"));
            BufferedImage actualImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + actualImageName + ".png"));

            ImageDiffer imgDiff = new ImageDiffer();

            ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
            //ImageIO.write(diff.getDiffImage(), "png", new File(System.getProperty("user.dir") + "\\Screenshots\\Difference.png"));


            if (diff.withDiffSizeTrigger(300000).hasDiff() == true) { //.withDiffSizeTrigger(90000)
                System.out.println(diff.getDiffSize());
                System.out.println("Images are different");

            } else {
                System.out.println("Images are same");
            }
            assertThat(diff.hasDiff()).isFalse();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getMapZoomLevel() {
        return map.getAttribute("data-zoom");
    }

    @Step("Verify that map is zoomed in")
    public void verifyThatMapIsZoomedIn(String previousZoomValue) {
        assertThat(Integer.parseInt(previousZoomValue)).isLessThan(Integer.parseInt(getMapZoomLevel()));
    }

    @Step("Verify that map is zoomed out")
    public void verifyThatMapIsZoomedOut(String previousZoomValue) {
        assertThat(Integer.parseInt(previousZoomValue)).isGreaterThan(Integer.parseInt(getMapZoomLevel()));
    }

    @Step("Get WebApp legend icons")
    public List<String> getWebAppLegendIcons() {
        //legendIcons.stream().forEach(item-> System.out.println(item.getAttribute("outerHTML")));
        return legendIcons.stream().map(item-> item.getAttribute("outerHTML")).collect(Collectors.toList());
    }

    @Step("Verify that Legend icons changed on Zoom In")
    public void verifyThatLegendsAreChangedOnZoomIn(int previousNoOfLegends) {
        System.out.println(previousNoOfLegends);
        System.out.println(getWebAppLegendIcons().size());
        assertThat(getWebAppLegendIcons().size()).isGreaterThan(previousNoOfLegends);
    }

    @Step("Verify that Legend icons changed on Zoom Out")
    public void verifyThatLegendsAreChangedOnZoomOut(int previousNoOfLegends) {
        sleep(2000);
        System.out.println(previousNoOfLegends);
        System.out.println(getWebAppLegendIcons().size());
        assertThat(getWebAppLegendIcons().size()).isLessThan(previousNoOfLegends);
    }

    @Step("Input title for the Point Note")
    public void inputPointNoteTitle(String inputTitle){
        pointNoteTitleTxtBox.sendKeys(inputTitle);
    }

    @Step("Input description for the Point Note")
    public void inputPointNoteDescription(String inputDescription){
        pointNoteDescriptionTxtBox.sendKeys(inputDescription);
    }

    @Step("Click on Save button to save the note")
    public void clickOnPointNoteSaveBtn(){
        pointNoteSaveBtn.click();
        sleep(1000);
    }

    @Step("Click on Delete button to delete the point note")
    public void clickOnPointNoteDeleteBtn(){
        pointMapNote.click();
        pointNoteDeleteBtn.click();
    }

    @Step("Click on Close button to close the note popup")
    public void clickOnPointNoteCloseBtn(){
        pointNoteCloseBtn.click();
    }

    @Step("Move point note to new location")
    public void movePointNoteToNewLocation(){
        action.dragAndDropBy(pointMapNote, -150, 100).perform();
        sleep(1000);
    }

    public String getXLocationOfPointNote(){
        return pointMapNote.findElement(By.cssSelector("circle")).getAttribute("cx");
    }

    public String getYLocationOfPointNote(){
        return pointMapNote.findElement(By.cssSelector("circle")).getAttribute("cy");
    }

    @Step("Verify that Point Note is moved to a new location")
    public void verifyThatPointNoteIsMovedToNewLocation(String previousX, String previousY){
        assertThat(getXLocationOfPointNote()).isNotEqualTo(previousX);
        assertThat(getYLocationOfPointNote()).isNotEqualTo(previousY);
    }

    public String getCoordinateInfo() {
        /*sleep(5000);
        moveMouseToElement(map);
        System.out.println(coordinateInfo.getText());*/
        return coordinateInfo.getText();
    }

    public void clickOnEnableCoordinateLocateBtn(){
            enableCoordinateLocateBtn.click();
    }

    public void clickOnTheCentreOfTheMap() {
        action.moveToElement(map).click().perform();
    }

    public void verifyThatEsriGreenPinIsDisplayedOnTheMap() {
        assertThat(esriGreenPin.getAttribute("xlink:href")).containsIgnoringCase("esriGreenPin");
    }

    public void verifyTheCoordinates(String expectedCoordinates) {
        assertThat(getCoordinateInfo()).isEqualTo(expectedCoordinates);
    }

    public int getNoOfFeaturesInJsonFile(String fileName)  {
        Map<String,Object> map = null;
        try {
            map = Utils.getMapFromJSONString(FileUtils.readFileToString(new File(DOWNLOAD_DIRECTORY_PATH + "\\" + fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> arr = (ArrayList<String>) map.get("features");
        System.out.println(map.get("features"));
        return arr.size();
    }

    /*public void verifyThatFileIsDownloaded(String fileName) {
        assertThat(Utils.doesFileExist(DOWNLOAD_DIRECTORY_PATH + "\\" + fileName)).isTrue();
    }*/

    public void verifyThatPopupIsDisplayed(String popupName) {
        assertThat(webAppPopup.getText()).isEqualToIgnoringCase(popupName);
    }

    public void closePopup(){
        webAppPopupOkBtn.click();
    }

    public void inputLayerNameInCreateLayerPopup(String name) {
        webappCreateLayerNameInput.sendKeys(name);
    }

    public void clickOnCreateOrSaveLayerPopupOkBtn() {
        createOrSaveLayerPopupOkBtn.click();
        wait.until(ExpectedConditions.invisibilityOf(createOrSaveLayerPopupOkBtn));
        sleep(5000);
    }

    public void inputTitleInSaveToMyContentPopup(String name) {
        saveToMyContentTitleTxtBox.sendKeys(name);
    }

    @Step("Click on Screening widget")
    public void clickOnScreeningWidget() {
        screeningWidget.click();
        sleep(5000);
    }

    @Step("Click on Query widget")
    public void clickOnQueryWidget() {
        queryWidget.click();
    }

    @Step("Click on Edit widget")
    public void clickOnEditWidget() {
        editWidget.click();
    }

    @Step("Select '{layerToSelect}' option in Feature layers drop down")
    public void selectEditWidgetFeatureLayersInDrpDwn(String layerToSelect) {
        WebElement drpDwn = driver.findElement(By.cssSelector("select.flDropDown"));
        Select select = new Select(drpDwn);
        drpDwn.click();
        select.selectByVisibleText(layerToSelect);
    }

    @Step("Select feature layer to add")
    public void selectFeatureLayerInEditWidget() {
        driver.findElement(By.cssSelector(".dojoxGridCell  .item")).click();
    }

    @Step("Input details in edit popup. Title as {title} and Description as {description}")
    public void inputEditPopupDetails(String title, String description) {
        driver.findElement(By.xpath("//td[text()='Title']/..//input[contains(@class,'dijitInputInner')]")).sendKeys(title);
        driver.findElement(By.xpath("//td[text()='Description']/..//input[contains(@class,'dijitInputInner')]")).sendKeys(description);
        sleep(1000);
    }

    @Step("Click on save button in edit popup")
    public void clickOnEditPopupSaveBtn() {
        driver.findElement(By.cssSelector(".save-button")).click();
        sleep(1000);
    }

    @Step("Close the edit popup")
    public void clickOnEditPopupCloseBtn() {
        driver.findElement(By.cssSelector(".close-button")).click();
    }

    @Step("Click on the Point just added on the map")
    public void clickOnPointLayerOnWebApp() {
        sleep(2000);
        driver.findElement(By.cssSelector("g[data-geometry-type='point'] circle")).click();
    }

    @Step("Verify the note at the bottom of the popup showing the details of the user who last edited")
    public void verifyTheEditorTrackingInfo() {
        assertThat(driver.findElement(By.cssSelector(".atiEditorTrackingInfo")).getText()).containsIgnoringCase("Edited by ra097");
        driver.findElement(By.cssSelector(".jimu-widget-edit-infoWindow .titleButton.close")).click();
    }

    @Step("Delete the point on the map")
    public void deleteEditPointOnWebApp() {
        driver.findElement(By.cssSelector(".atiDeleteButton")).click();
        sleep(2000);
    }

    @Step("Refresh the webapp")
    public void refresh() {
        driver.get(driver.getCurrentUrl());
        sleep(8000);
    }
}
