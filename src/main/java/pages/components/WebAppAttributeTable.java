package pages.components;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePage;
import utils.Utils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WebAppAttributeTable extends BasePage {

    @FindBy(css = "div.jimu-widget-attributetable-main")
    WebElement attributeTable;

    //@FindBy(css = "span[widgetid='dijit_form_DropDownButton_0'] .dijitButtonNode")
    //@FindBy(xpath = "//span[contains(text(),'Options')]")
    //@FindBy(css = "//div[contains(@class, 'dijitVisible')]//span[contains(text(),'Options')]")
    @FindBy(css = ".dijitVisible .esriAttributeTableOptionsImage")
    WebElement optionsDrpDwn;

    @FindBy(css = ".dijitVisible .dijitNoIcon")
    WebElement filterByMapExtentBtn;

    @FindBy(css = ".add-expression")
    WebElement filterPopupAddExpressionBtn;

    @FindBy(css = ".allExpsBox  div[id*='widget_dijit_form_FilteringSelect'] .dijitArrowButton")
    WebElement filterSelectFieldDrpDwn;

    @FindBy(css = "div[role='option']")
    List<WebElement> filterFieldOptions;

    @FindBy(css = ".jimu-filter-simple-value-provider input[data-dojo-attach-point*='textbox']")
    WebElement fieldValueTextBox;

    @FindBy(css = ".jimu-icon-delete")
    WebElement filterDeleteBtn;

    @FindBy(css = "div.jimu-popup-action-btn[title='OK']")
    WebElement popupOkBtn;

    @FindBy(css = ".jimu-widget-attributetable-feature-table-footer .self-footer")
    List<WebElement> attributeTableFooterTexts;

    @FindBy(css = ".dgrid-scroller .dgrid-row-table tr td")
    WebElement attributeTableRow;

    @FindBy(css = "div[title='Hide Attribute Table']")
    WebElement hideAttributeTableBtn;

    @FindBy(css = ".dijitVisible div.jimu-widget-attributetable-feature-table .dgrid-hider-menu label")
    List<WebElement> showHideColumnsList;

    @FindBy(css = ".dijitVisible button.dgrid-hider-toggle")
    WebElement showHideColumnToggleBtn;

    @FindBy(css = ".dijitVisible .dgrid-header-row th")
    List<WebElement> tableHeaders;

    @FindBy(css = ".dijitVisible .dijitButtonNode")
    List<WebElement> attributeTableButtons;


    WebDriver driver;
    WebDriverWait wait;
    Actions action;

    public WebAppAttributeTable(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.action = new Actions(driver);
    }

    public void clickOnButtonInAttributeTable(String buttonName) {
        attributeTableButtons.stream().filter(button -> button.getText().equalsIgnoreCase(buttonName)).findFirst().get().click();
        sleep(1000);
    }

    @Step("Click on Options dropdown")
    public void clickOnOptions() {
        sleep(3000);
        optionsDrpDwn.click();
    }

    @Step("Click on Filter By Map Extent")
    public void clickOnFilterByMapExtent() {
        sleep(2000);
        clickOnElementWithAdvancedJS(filterByMapExtentBtn);
        sleep(15000);
    }

    @Step("Select option {option}")
    public void selectOption(String option) {
        driver.findElement(By.cssSelector("table.dijitMenuTable .dijitMenuItem[aria-label='"+option+" ']")).click();
        sleep(1000);
    }

    @Step("Click on 'Add Expression'")
    public void addFilterExpression() {
        filterPopupAddExpressionBtn.click();
    }

    @Step("Select the filed {fieldToSelect} from the dropdown")
    public void selectFilterField(String fieldToSelect){
        sleep(3000);
        filterSelectFieldDrpDwn.click();
        filterFieldOptions.stream().filter(option -> option.getText().contains(fieldToSelect)).findFirst().get().click();
    }

    @Step("Input value {fieldValue} into the filter value provider")
    public void provideFieldValue(String fieldValue) {
        fieldValueTextBox.sendKeys(fieldValue);
        sleep(1000);
    }

    @Step("Delete the filter")
    public void deleteFilter() {
        filterDeleteBtn.click();
        sleep(1000);
    }

    @Step("Click on Ok button")
    public void clickOnOKBtn() {
        popupOkBtn.click();
        sleep(3000);
    }

    public String getNoOfFeaturesTextInAttributeTable() {
        return attributeTableFooterTexts.stream().filter(text -> text.getText().contains("features")).findFirst().get().getText();
    }

    public int getTotalNoOfFeaturesInAttributeTable(){
        return Integer.parseInt(getNoOfFeaturesTextInAttributeTable().split(" ")[0]);
    }

    @Step("Verify that no. of features displayed on the map are decreased")
    public void verifyThatNoOfFeaturesDisplayedInAttributeTableAreDecreased(int previousNoOfFeatures) {
        assertThat(getTotalNoOfFeaturesInAttributeTable()).isLessThan(previousNoOfFeatures);
    }

    @Step("Verify the no. of features displayed in the attribute table is equal to {expectedNoOfFeatures}")
    public void verifyThatNoOfFeaturesDisplayedInAttributeTableIsEqualTo(int expectedNoOfFeatures) {
        assertThat(getTotalNoOfFeaturesInAttributeTable()).isEqualTo(expectedNoOfFeatures);
    }

    @Step("Verify that no. of features displayed on the map are increased")
    public void verifyThatNoOfFeaturesDisplayedInAttributeTableAreIncreased(int previousNoOfFeatures) {
        assertThat(getTotalNoOfFeaturesInAttributeTable()).isGreaterThan(previousNoOfFeatures);
    }

    @Step("Verify that filter is applied")
    public void verifyThatNoOfFeaturesAreOne() {
        assertThat(getNoOfFeaturesTextInAttributeTable()).contains("1 features");
    }

    @Step("Verify that filter is deleted")
    public void verifyThatFilterIsDeletedByCheckingNoOfFeatures() {
        assertThat(getTotalNoOfFeaturesInAttributeTable()).isGreaterThan(1);
    }

    @Step("Double click on the row in the attribute table")
    public void doubleClickOnTheAttributeRow() {
        action.doubleClick(attributeTableRow).perform();
        sleep(5000);
    }

    @Step("Hide Attribute Table")
    public void clickOnHideAttributeTable(){
        hideAttributeTableBtn.click();
    }

    public void deselectColumnsFromAttributeTable(List<String> columnsToHide) {
        showHideColumnsList.stream().filter(column -> columnsToHide.contains(column.getText())).collect(Collectors.toList())
        .stream().forEach(columnElementToHide -> columnElementToHide.findElement(By.xpath("./../input")).click());
        showHideColumnToggleBtn.click();
    }

    public void verifyThatHeadersAreNotDisplayedInAttributeTable(List<String> headers) {
        headers.stream().forEach(header -> assertThat(driver.findElement(By.xpath("//th/div[contains(text(),'"+header+"')]")).isDisplayed()).isFalse());
    }

    public WebElement getAttributeTableHeaderColumn(String columnName) {
        return tableHeaders.stream().filter(header -> header.getText().contains(columnName)).findFirst().get();
    }

    public void sortColumn(String columnName, String order) {
        sleep(2000);
        getAttributeTableHeaderColumn(columnName).click();
        driver.findElement(By.xpath("//div[contains(@class,'dijitMenuPopup')]//*[contains(text(),'"+order+"')]")).click();
        sleep(5000);
    }

    public List<String> getAllValuesForColumn(String columnName) {
        List<WebElement> columnValues = driver.findElements(By.cssSelector(".ui-widget-content.dgrid-content .dgrid-row .field-"+columnName.replaceAll(" ", "_").toUpperCase()));
        System.out.println("all values: " + columnValues.size());
        return columnValues.stream().map(column->column.getText()).collect(Collectors.toList());
    }

    public void verifyThatColumnIsSortedInDescendingOrder(List<String> columnValuesBeforeSort, List<String> columnValuesAfterSort) {
        System.out.println("Before: " + columnValuesBeforeSort.size());
        System.out.println("After: " + columnValuesAfterSort.size());
        columnValuesBeforeSort.stream().forEach(System.out::println);
        columnValuesAfterSort.stream().forEach(System.out::println);

        assertThat(columnValuesBeforeSort).isNotSameAs(columnValuesAfterSort);
        Collections.sort(columnValuesBeforeSort, (a, b) -> Integer.parseInt(a) < Integer.parseInt(b) ? -1 : a == b ? 0 : 1);
        assertThat(columnValuesBeforeSort).containsExactlyElementsOf(columnValuesAfterSort);
        assertThat(columnValuesBeforeSort).isSameAs(columnValuesAfterSort);
    }

    public void verifyThatColumnIsSortedInDescendingOrder(List<String> columnValuesAfterSort) {
        columnValuesAfterSort.removeIf(value -> value.trim().isEmpty());
        assertThat(Utils.isSortedDescending(columnValuesAfterSort)).isTrue();
    }

    public void verifyThatColumnIsSortedInAscendingOrder(List<String> columnValuesAfterSort) {
        columnValuesAfterSort.removeIf(value -> value.trim().isEmpty());
        assertThat(Utils.isSortedAscending(columnValuesAfterSort)).isTrue();
    }

    public void swapColumnPositionsFor(String columnName1, String columnName2) {
        WebElement column1 = getAttributeTableHeaderColumn(columnName1);
        WebElement column2 = getAttributeTableHeaderColumn(columnName2);
        action.dragAndDrop(column1, column2).perform();
        sleep(5000);
    }

    public void verifyThatColumnPositionIsChanged(String movedColumnName, String targetColumnName) {
        WebElement movedColumn = tableHeaders.stream().filter(header -> header.getText().contains(movedColumnName)).findFirst().get();
        WebElement targetColumn = tableHeaders.stream().filter(header -> header.getText().contains(targetColumnName)).findFirst().get();
        assertThat(tableHeaders.indexOf(movedColumn)).isEqualTo(tableHeaders.indexOf(targetColumn)-1);
    }


}
