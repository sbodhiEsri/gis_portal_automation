package pages.components;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePage;
import utils.Utils;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ScreeningWidgetPopup extends BasePage {

    @FindBy(css = ".jimu-widget-screening-drawTool div[title='Point']")
    WebElement pointDrawMode;

    @FindBy(css = ".jimu-widget-screening-drawTool")
    WebElement screeningWidgetPopupElem;

    @FindBy(css = "div.esriCTBufferDistanceLabelTextBoxContainer input.dijitInputInner")
    WebElement bufferDistanceTxtBox;

    @FindBy(css = "table.esriCTDistanceUnit .dijitButtonText")
    WebElement bufferUnitDrpDwn;

    @FindBy(css = ".dijitMenuPopup .dijitMenuItem")
    List<WebElement> drpDwnOptions;

    @FindBy(css = "div.esriCTShowReportsButton")
    WebElement reportBtn;

    @FindBy(css = "div.esriCTDownloadContainer")
    WebElement downloadBtn;

    @FindBy(css = "span.esriCTPrintContainer ")
    WebElement printBtn;

    @FindBy(css = "table.esriCTPrintLayoutDropdown .dijitButtonText")
    WebElement printPopupLayoutDrpDwn;

    @FindBy(css = ".dijitTooltipDialogPopup button.esriCTDownloadSettingsBtn")
    WebElement popupDownloadBtn;

    @FindBy(css = ".esriCTPopupSettingsBtnContainer .jimu-btn[title='Print']")
    WebElement popupPrintBtn;

    @FindBy(css = "div.jimu-widget-screening div[aria-label='Select All'] .checkbox")
    WebElement selectAllCheckboxForLayerFields;

    @FindBy(css = ".jimu-popup-action-btn[title='OK']")
    WebElement selectFieldsPopupOkBtn;

    @FindBy(css = "div.esriCTLayerTitleAndFieldParentContainer")
    List<WebElement> networkReportLayers;

    @FindBy(css = "div.esriCTBackButton")
    WebElement backButton;

    @FindBy(css = "div.esriCTShapefileButton ")
    WebElement shapeFileTab;

    @FindBy(css = "input[title='Upload']")
    WebElement uploadBtn;


    WebDriver driver;
    WebDriverWait wait;
    Actions action;

    public ScreeningWidgetPopup(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.action = new Actions(driver);
    }

    @Step("Select draw mode {modToSelect}")
    public void clickOnDrawMode(String modeToSelect) {
        sleep(2000);
        screeningWidgetPopupElem.findElement(By.cssSelector("div[title='" + modeToSelect + "'")).click();
    }

    @Step("Input buffer distance {distanceValue}")
    public void inputBufferDistance(String distanceValue) {
        bufferDistanceTxtBox.clear();
        bufferDistanceTxtBox.sendKeys(distanceValue);
    }

    private void selectDropDwnOption(String optionToSelect) {
        drpDwnOptions.stream().filter(elem -> elem.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
    }

    @Step("select buffer unit as {unitToSelect}")
    public void selectBufferUnit(String unitToSelect) {
        bufferUnitDrpDwn.click();
        selectDropDwnOption(unitToSelect);
    }

    @Step("Click on Report button")
    public void clickOnReportBtn() {
        sleep(2000);
        //reportBtn.click();
        clickOnElementWithAdvancedJS(reportBtn);
        sleep(10000);
    }

    @Step("Click on Download button")
    public void clickOnDownloadBtn() {
        sleep(10000);
        //wait.until(ExpectedConditions.elementToBeClickable(downloadBtn));
        downloadBtn.click();
        popupDownloadBtn.click();
        sleep(2000);
    }

    @Step("Click on Print button")
    public void clickOnPrintBtn() {
        printBtn.click();
    }

    @Step("Select print layout {layoutToSelect}")
    public void selectPrintLayout(String layoutToSelect) {
        printPopupLayoutDrpDwn.click();
        selectDropDwnOption(layoutToSelect);
    }

    public void clickOnPopupPrintBtn() {
        popupPrintBtn.click();
        sleep(8000);
    }

    @Step("Verify that Printable Report is opened in a new window")
    public void verifyThatPrintReportIsOpenedInNewWindow(){
        Set<String> currentWindowHandles = driver.getWindowHandles();
        assertThat(currentWindowHandles.size()).isEqualTo(3);
        switchToNewWindow();
        System.out.println(driver.getCurrentUrl());
        switchToPreviousWindow();
    }

    @Step("Click on Settings for layer {layerName}")
    public void clickOnSettingsForLayerInPopup(String layerName) {
        driver.findElement(By.cssSelector("div[title='"+ layerName +"'] + div[title='Choose attributes to display']")).click();
    }

    @Step("Select fields {fieldsToSelect}")
    public void selectFieldsForTheLayer(String fieldsToSelect) {
        List<String> fieldsList = Arrays.asList(fieldsToSelect.split(","));
        fieldsList.stream().forEach(System.out::println);
        selectAllCheckboxForLayerFields.click();
        fieldsList.stream().forEach(field ->
                driver.findElement(By.cssSelector("div.jimu-widget-screening div[aria-label='"+ field.trim() +"'] .checkbox")).click());
        selectFieldsPopupOkBtn.click();
    }

    public WebElement getNetworkReportLayer(String layerName) {
        return networkReportLayers.stream().filter(layer-> layer.getText().contains(layerName)).findFirst().get();
    }

    public List<String> getFieldsDisplayedForLayer(String layerName) {
        WebElement layer = getNetworkReportLayer(layerName).findElement(By.xpath("./.."));
        layer.findElement(By.cssSelector("div.esriCTLayerPanelIcon")).click();
        List<WebElement> fieldNames = layer.findElements(By.cssSelector(".esriCTImpactSummaryLayerDetailContainer table[esricttableindex='0'] .esriCTAttrName"));
        return fieldNames.stream().map(field -> field.getText()).collect(Collectors.toList()).stream().filter(field -> !field.trim().isEmpty()).collect(Collectors.toList());
    }

    @Step("Verify that fields {expectedFields} are displayed under selected layer {layerName}")
    public void verifyTheFieldsDisplayedForSelectedLayer(String expectedFields, String layerName) {
        List<String> fieldsList = Arrays.asList(expectedFields.split(","));
        assertThat(getFieldsDisplayedForLayer(layerName)).containsExactlyElementsOf(fieldsList.stream().map(field -> field.trim()+":").collect(Collectors.toList()));
    }

    @Step("Click on Back button")
    public void clickOnBackBtn() {
        backButton.click();
    }

    @Step("Click on Shapefile tab")
    public void clickOnShapeFileTab() {
        shapeFileTab.click();
        sleep(10000);
    }

    public void clickOnUploadBtn(){
        sleep(5000);
        //uploadBtn.click();
        action.click(driver.findElement(By.cssSelector("div.esriCTUploadShapefileButtonDiv .esriCTUploadShapefileButton"))).perform();
        //clickOnElementWithJS(driver.findElement(By.cssSelector("div.esriCTUploadShapefileButtonDiv input")));
        sleep(5000);
    }

    public void uploadShapeFile(String fileName) {
        try {
            Utils.provideFileNameInUploadFileDialog(fileName);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        sleep(10000);
    }
}
