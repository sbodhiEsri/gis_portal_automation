package pages.components;

import org.openqa.selenium.By;

public interface LayerMenu {

    By layerMenu = By.id("layerMenu");
    By zoomTo = By.id("layerMenu.zoomTo");

    By transparency = By.id("layerMenu.layerTransp");
    By transparencyValue = By.xpath("//table[@id = 'layerMenu.opacitySlider']//input");
    By transparencySlider = By.id("layerMenu.opacitySlider");

    By visibilityRange = By.id("layerMenu.scale");
    By visibilityDrpDwn = By.id("dijit_form_DropDownButton_0");
    By visibilityDrpDwnOpts = By.cssSelector("#esri_dijit_VisibleScaleRangeSlider_ScaleMenu_0 li");
    By visibilitySlider = By.id("esri_dijit_VisibleScaleRangeSlider__SlideEvent_0");

    By moveUp = By.id("layerMenu.moveUp");
    By moveDown = By.id("layerMenu.moveDown");
    By rename = By.id("layerMenu.rename");
    By copy = By.id("layerMenu.copy");
    By layerNameTxtBox = By.cssSelector("#widget_layer_title input");
    By hideInLegend = By.id("layerMenu.disableShowLegend");
    By showInLegend = By.id("layerMenu.enableShowLegend");
    By remove = By.id("layerMenu.remove");
    By removePopup = By.id("layerMenu.removePopup");
    By enablePopup = By.id("layerMenu.enablePopup");
    By configurePopup =  By.id("layerMenu.configPopup");
    By showPopupCheckbox = By.cssSelector(".showPopup input");
    By createLabels = By.id("layerMenu.editLabels");
    By saveLayer = By.id("layerMenu.saveProps");
    By saveLayerAsItem = By.id("layerMenu.saveAsItem");
    By refreshInterval = By.id("layerMenu.refreshInterval");
    By refreshIntervalCheckBox = By.xpath("//div[@id='layerMenu.refreshIntervalDlg']//input[@type='checkbox']");
    By refreshIntervalTxtBox = By.xpath("//div[@id='layerMenu.refreshIntervalDlg']//input[@type='text'][contains(@id,'dijit_form_NumberTextBox')]");
    By showItemDetails = By.id("layerMenu.layerInfo");
}
