package pages.components;

import org.openqa.selenium.By;

public interface FeatureAttributeTable {

    By attributeTableTitle = By.cssSelector(".esri-feature-table-title");
    By attributeTableOptions = By.cssSelector("div[title='Options']");
    By filterOption = By.cssSelector("tr[aria-label='Filter ']");
    By addFieldOption = By.cssSelector("tr[aria-label='Add Field ']");
    By centerOnSelectionOption = By.cssSelector("tr[aria-label='Center on Selection ']");
    By clearSelectionOption = By.cssSelector("tr[aria-label='Clear Selection ']");
    By addFieldPopupFieldNameTxtBox = By.xpath("//td/label[contains(text(),'Field')]/../..//input[contains(@id, 'TextBox')]");//By.cssSelector(".esriAddFormTable tr:nth-child(2) input[id*='dijit_form_ValidationTextBox']");//By.id("dijit_form_ValidationTextBox_0");
    By addFieldPopupDisplayNameTxtBox = By.xpath("//td/label[contains(text(),'Display')]/../..//input[contains(@id, 'TextBox')]");//By.cssSelector(".esriAddFormTable tr:nth-child(3) input[id*='dijit_form_ValidationTextBox']");//By.id("dijit_form_ValidationTextBox_1");
    By addFieldPopupAddBtn = By.xpath("//span[contains(text(),'Add New Field')]");
    By savingLayerPopup = By.id("waiting-dialog-content");
    By removeFilterBtnOnPopup = By.cssSelector("span[widgetid='removeFilterButton'] .dijitButtonNode");
    By attributeTableFieldName = By.cssSelector("th.field-fieldname");
    By attributeTableFieldValue = By.cssSelector("td.field-fieldname");
    By getAttributeTableFirstRow = By.cssSelector(".dgrid-row");
    By attributeTablePopupAttrName = By.cssSelector(".contentPane .attrTable .attrName");
    By attributeTablePopupAttrValue = By.cssSelector(".contentPane .attrTable .attrValue");
    By attributeTableHeaderCurrency = By.cssSelector(".field-CURRENCY .esri-feature-table-column-header-title");
    By attributeTableHeaderCurrencySort = By.cssSelector(".field-CURRENCY .dgrid-resize-header-container");
    By fieldCurrencyValue = By.cssSelector("td.field-CURRENCY");
    By closeBtn = By.id("tableCloseButton");

}
