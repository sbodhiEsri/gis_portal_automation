package pages.components;

import org.openqa.selenium.By;

public interface WebAppLayerMenuOptions {

    By visibilityRange = By.cssSelector(".menu-item[itemid='setVisibilityRange']");
    By visibilityDrpDwn = By.id("dijit_form_DropDownButton_0");
    By visibilityDrpDwnOpts = By.cssSelector("#esri_dijit_VisibleScaleRangeSlider_ScaleMenu_0 li");

    By zoomTo = By.cssSelector(".menu-item[itemid='zoomto']");

    By viewInAttributeTable = By.cssSelector(".menu-item[itemid='table']");

}
