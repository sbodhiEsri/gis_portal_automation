package pages.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MyGroups {

    @FindBy(css = ".js-filter-category-container div.js-category-filter a.filter-tree-link")
    List<WebElement> categories;

    @FindBy(css = "#uniqName_1_3 a")
    List<WebElement> itemTypes;

    WebDriver driver;
    WebDriverWait wait;

    public MyGroups(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    public void selectFilterCategory(String categoryToBeSelected) {
        categories.stream().filter(
                category -> category.getAttribute("data-category-name")
                        .equalsIgnoreCase(categoryToBeSelected)).findFirst().get().click();
    }

    public void selectFilterItemType(String itemType) {
        itemTypes.stream().forEach(item -> System.out.println(item.getText()));
        itemTypes.stream().filter(
                item -> item.getText()
                        .equalsIgnoreCase(itemType)).findFirst().get().click();
    }

    public void selectMap(String mapName) {
        driver.findElement(By.cssSelector("a[title='" + mapName + "']")).click();
    }
}
