package pages.components;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class WebMapPage extends BasePage {

    @FindBy(css = "a.js-open-mapViewer:nth-child(2)")
    WebElement openInMapViewerBtn;

    @FindBy(css = "a.js-open-mapViewer.js-open-btn")
    WebElement openInMapViewerBtn2;

    @FindBy(css = "button[aria-label='Open menu']")
    WebElement openMenu;

    @FindBy(css = ".dropdown-menu-full.dropdown-right>a")
    List<WebElement> openMenuList;

    @FindBy(css = ".dropdown-menu-full.dropdown-right>a.add-to-new-viewer")
    WebElement addToNewMapOption;

    @FindBy(css = "a[data-tab='overview']")
    WebElement overviewTab;

    @FindBy(css = ".js-layers a")
    List<WebElement> layersList;

    @FindBy(css = "#url .input-group-input.copy-to-clipboard-input")
    WebElement layerURL;

    @FindBy(id = "pagetitle")
    WebElement layerTitle;

    @FindBy(css = ".js-view-url")
    WebElement viewURL;

    @FindBy(css = ".titlecell")
    WebElement arcGISRestDirectory;

    @FindBy(css = "div.if-can-export button.btn")
    WebElement exportDrpDwnBtn;

    @FindBy(css = ".if-can-export .can-export button")
    List<WebElement> exportOptions;

    @FindBy(css = "#export-dialog .dijitReset .dijitInputField input[id*='TextBox']:not([maxlength])")
    WebElement exportPopupTitleTxtBox;

    @FindBy(css = "#export-dialog .esriTags .dijitInputField input")
    WebElement exportPopupTags;

    @FindBy(css = ".primary .dijitButtonNode")
    WebElement exportBtn;

    @FindBy(css = "button.if-file")
    WebElement downloadBtn;

    @FindBy(css = ".esriFloatTrailing  span")
    WebElement exportingLabel;

    WebDriver driver;
    WebDriverWait wait;
    Actions action;

    public WebMapPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.action = new Actions(driver);
    }

    @Step("Verify that Overview tab is opened")
    public void verifyThatOverviewTabIsOpened() {
        wait.until(ExpectedConditions.attributeContains(overviewTab, "class", "is-active"));
        assertThat(overviewTab.getAttribute("class").contains("is-active")).isTrue();
    }

    @Step("Click on Open in Map Viewer")
    public void clickOnOpenInMapViewer() {

        WebElement mapViewerBtn;
        try {
            mapViewerBtn = wait.until(ExpectedConditions.elementToBeClickable(openInMapViewerBtn));
        } catch (TimeoutException timeout) {
            mapViewerBtn = wait.until(ExpectedConditions.elementToBeClickable(openInMapViewerBtn2));
        }
        mapViewerBtn.click();
    }

    public void clickOnOpenMenu() {
        /*wait.until(ExpectedConditions.elementToBeClickable(openMenu));
        action.moveToElement(openMenu).click().perform();*/
        WebElement menu = driver.findElement(By.cssSelector(".dropdown button.js-dropdown-toggle.btn-split-right"));
        //action.moveToElement(menu).moveToElement(menu).click().build().perform();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                menu,
                "aria-expanded",
                "true");

        /*menu.sendKeys(Keys.ENTER);
        action.moveToElement(menu).doubleClick().perform();*/
    }

    public void selectOpenMenuOption(String optionToSelect) {
        //wait.until(ExpectedConditions.visibilityOfAllElements(openMenuList));
        openMenuList.stream().forEach(option -> System.out.println(option.getText()));
        openMenuList.stream().filter(option -> option.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
    }

    public void clickOnAddToNewMap() {
        addToNewMapOption.click();
    }

    public WebElement getLayerFromLayersList(String layerName) {
        return layersList.stream().filter(layer -> layer.getText().equalsIgnoreCase(layerName)).findFirst().get();
    }

    @Step("Open a layer {layerName}")
    public void clickAndOpenLayerFromList(String layerName) {
        sleep(400);
        getLayerFromLayersList(layerName).click();
    }

    @Step("Verify that layer is opened in a new window")
    public void verifyThatLayerIsOpenedInNewWindow() {
        assertThat(driver.getWindowHandles().size()).isEqualTo(2);
        switchToNewWindow();
        sleep(1000);
    }

    @Step("Verify the title of the layer is {expectedTitle}")
    public void verifyTitleOfTheLayerOpenedInNewWindow(String expectedTitle) {

        assertThat(layerTitle.getText()).isEqualToIgnoringCase(expectedTitle);
    }

    public String getLayerURL(){
        return layerURL.getAttribute("value");
    }

    @Step("Click on view link next to URL")
    public void clickOnViewLink(){
        viewURL.click();
        sleep(500);
    }

    @Step("Verify that ARC GIS REST Service Directory is opened in new window")
    public void verifyThatGIS_REST_DirectoryIsOpened(String expectedTitle) {
        assertThat(driver.getWindowHandles().size()).isEqualTo(2);
        switchToNewWindow();
        sleep(1000);
        assertThat(arcGISRestDirectory.getText()).isEqualToIgnoringCase(expectedTitle);
    }

    @Step("Verify that layer is pointing to Sandpit")
    public void verifyThatLayerIsPointingToCorrectEnv(String envName) {
        assertThat(getLayerURL()).containsIgnoringCase(envName);
    }

    @Step("Verify that Native Title layer is pointing to Arc GIS online service")
    public void verifyThatNativeTitleLayerIsPointingToArcGISOnlineService() {
        assertThat(getLayerURL()).containsIgnoringCase("https://services2.arcgis.com/rzk7fNEt0xoEp3cX/ArcGIS/rest/services/NNTT_Custodial_AGOL/FeatureServer");
    }

    public void closeTheLayerTab(){
        driver.close();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
    }

    @Step("Click on Export button on the right hand side")
    public void clickOnExportDropDwn() {
        exportDrpDwnBtn.click();
    }

    @Step("Choose file type {optionToChoose} from the drop down options")
    public void chooseOptionToExport(String optionToChoose) {
        exportOptions.stream().filter(option -> option.getAttribute("data-type").equalsIgnoreCase(optionToChoose)).findFirst().get().click();
    }

    @Step("Input export file title as {inputTitle}")
    public void inputExportFileTitle(String inputTitle) {
        exportPopupTitleTxtBox.clear();
        exportPopupTitleTxtBox.sendKeys(inputTitle);
        sleep(500);
    }

    @Step("Input export file tags as {inputTags}")
    public void inputExportFileTags(String inputTags) {
        exportPopupTags.clear();
        exportPopupTags.sendKeys(inputTags);
        exportPopupTags.sendKeys(Keys.ENTER);
    }

    @Step("Click on Export button in the popup")
    public void clickOnExportBtn() {
        exportBtn.click();
        wait.until(ExpectedConditions.invisibilityOf(exportingLabel));
        sleep(10000);
    }

    @Step("Click on Download button for the exported file")
    public void clickOnDownloadBtn() {
        downloadBtn.click();
        sleep(2000);
    }
}
