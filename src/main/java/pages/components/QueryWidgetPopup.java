package pages.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryWidgetPopup extends BasePage {

    @FindBy(css = "table.list-table .list-item-name")
    List<WebElement> layerList;

    @FindBy(css = ".checkedNameDiv")
    WebElement queryCriteriaAssetTypeDrpDwn;

    @FindBy(css = ".jimu-multiple-items-list .item")
    List<WebElement> dropDwnMenuItems;

    @FindBy(css = ".jimu-filter-value-provider")
    List<WebElement> queryCriteriaFields;

    @FindBy(css = "div.btn-execute")
    WebElement queryApplyBtn;

    @FindBy(css = "div.results-number .number-container")
    WebElement noOfFeaturesInResult;

    @FindBy(css = "div.query-result-action-button")
    WebElement queryResultActionBtn;

    @FindBy(css = "div.popup-menu .popup-menu-item")
    List<WebElement> queryResultActionMenuItems;

    @FindBy(css = ".line .input-item .dijitInputField  input.dijitInputInner:not([role])")
    WebElement saveToMyContentPopupTitle;

    @FindBy(css = ".jimu-popup-action-btn[title='OK']")
    WebElement saveToMyContentPopupOKBtn;

    @FindBy(css = "div.no-result-section")
    WebElement noResultSection;

    WebDriver driver;
    WebDriverWait wait;
    Actions action;

    public QueryWidgetPopup(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.action = new Actions(driver);
    }

    public void selectLayerInQueryWidget(String layerName) {
        layerList.stream().filter(layer -> layer.getText().equalsIgnoreCase(layerName)).findFirst().get().click();
    }

    public void selectQueryCriteriaAssetType(String optionToSelect) {
        queryCriteriaAssetTypeDrpDwn.click();
        dropDwnMenuItems.stream().filter(item -> item.getText().contains(optionToSelect)).findFirst().get().click();
    }

    public void provideInputForCriteria(String fieldName, String fieldValue) {
        WebElement inputTxtBox = queryCriteriaFields.stream().filter(field -> field.getText().contains(fieldName)).findFirst().get().findElement(By.cssSelector(".dijitInputInner"));
        scrollToElementWithJS(inputTxtBox);
        sleep(1000);
        inputTxtBox.sendKeys(fieldValue);
    }

    public void clickOnApplyBtn() {
        queryApplyBtn.click();
        sleep(1000);
    }

    public int getNoOfFeaturesDisplayedInQueryResult() {
        return Integer.parseInt(noOfFeaturesInResult.getText().split("/")[1]);
    }

    public void clickOnActionBtnOnQueryResultsPopup() {
        queryResultActionBtn.click();
        sleep(5000);
    }

    public void selectActionFromPopupMenu(String actionToSelect) {
        queryResultActionMenuItems.stream().filter(item -> item.getText().contains(actionToSelect)).findFirst().get().click();
        sleep(2000);
    }

    public void inputTitleInSaveToMyContentPopup(String title) {
        saveToMyContentPopupTitle.sendKeys(title);
    }

    public void clickOnOkInSaveToMyContentPopup() {
        saveToMyContentPopupOKBtn.click();
    }

    public void verifyThatNoQueryResultsAreDisplayed() {
        assertThat(noResultSection.isDisplayed()).isTrue();
    }
}
