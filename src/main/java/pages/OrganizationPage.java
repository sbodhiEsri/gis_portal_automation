package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.CustomCondition;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class OrganizationPage extends HeaderPage {

    @FindBy(css = ".js-action-inviteMembers")
    WebElement addMembers;

    @FindBy(css = ".js-action-inviteMembers")
    List<WebElement> addMembers_list;

    @FindBy(css = "input[value='add-saml']")
    WebElement addSAML;

    @FindBy(css = "input[value='add-built-in']")
    WebElement addBuiltIn;

    @FindBy(css = "button[title='New member']")
    WebElement newMemberBtn;

    @FindBy(css = "button[title='New members from a file']")
    WebElement newMembersFromFileBtn;

    @FindBy(css = ".js-file-add-btn")
    WebElement addFileBtn;

    @FindBy(name = "firstname")
    WebElement firstNameInput;

    @FindBy(name = "lastname")
    WebElement lastNameInput;

    @FindBy(name = "email")
    WebElement emailInput;

    @FindBy(name = "username")
    WebElement usernameInput;

    @FindBy(name = "password")
    WebElement passwordInput;

    @FindBy(css = "form .license-type__selected-option")
    WebElement userTypeDrpDwn;

    @FindBy(css = "form .license-type-option .license-type-option__desc-container")
    List<WebElement> userTypeDrpDwnOptions;

    @FindBy(css = ".invite-members-form__role-select-wrapper .dijitArrowButtonInner")
    WebElement userRoleDrpDwn;

    @FindBy(css = ".dijitMenu .dijitMenuItem[item]")
    List<WebElement> userRoleDrpDwnOptions;

    @FindBy(css = ".invite-members-form--add-btn")
    WebElement addBtn;

    @FindBy(css = ".js-overlay-primary")
    WebElement nextBtn;

    @FindBy(css = "a[href='#members']")
    WebElement membersTab;

    @FindBy(css = ".control-bar__search-input")
    WebElement searchBox;

    @FindBy(css = ".js-toggle-search-dropdown")
    WebElement searchMemberDrpDwn;

    @FindBy(css = ".js-search-options button[data-property='username']")
    WebElement searchMemberByUsernameBtn;

    @FindBy(css = ".org-member-full-name a")
    WebElement firstSearchResult;

    @FindBy(css = ".invite-members__list-item-label > span:first-child")
    WebElement userName;

    @FindBy(css = ".invite-members__list-item-label > span:first-child")
    List<WebElement> userNames;

    @FindBy(css = ".js-groups-tab-btn")
    WebElement groupsBtn;

    @FindBy(css = ".gb-search-area__input")
    WebElement groupSearchTextBox;

    @FindBy(css = ".block-group .group-card-content a")
    List<WebElement> userGroups;

    @FindBy(css = ".profileLinks a:first-child")
    WebElement profileLink;

    @FindBy(css = ".invite-members__field-row--has-errors")
    List<WebElement> errorFields;

    @FindBy(css = ".overlay-close")
    WebElement closeAddMembersPage;

    @FindBy(css = ".btn.leader-quarter")
    WebElement memberOptionsBtn;

    @FindBy(css = ".dropdown-right .dropdown-link:not(.hide)")
    List<WebElement> memberOptions;

    @FindBy(css = "#button_delete-warning-submit_label")
    WebElement deleteMemberBtn;

    /*@FindBy(css = ".js-add-to-groups-btn")
    WebElement addToGroupsBtn;

    @FindBy(css = ".searchControls input")
    WebElement addToGroupsPopupSearchBox;

    @FindBy(css = "a.linkNoHref")
    List<WebElement> addToGroupsPopupGroupList;

    @FindBy(css = ".btn-blue")
    WebElement addToGroupsPopupAddBtn;*/

    @FindBy(css = "input[name='licenseType']")
    List<WebElement> userTypeInputs;

    @FindBy(css = ".dijitArrowButtonInner")
    WebElement roleDrpDwn;

    @FindBy(css = ".dijitComboBoxMenu .dijitMenuItem")
    List<WebElement> roleTypesDrpDwnOpts;

    @FindBy(css = ".js-member-card-username")
    List<WebElement> newestMembersList;

    @FindBy(css = ".js-latest-content .card-title")
    List<WebElement> latestContentList;

    public OrganizationPage() {
        PageFactory.initElements(driver, this);
    }


    public void searchAndSelectGroup(String groupName) {
        groupSearchTextBox.clear();
        groupSearchTextBox.sendKeys(groupName);
        groupSearchTextBox.sendKeys(Keys.ENTER);
        sleep(1000);
        driver.findElement(By.cssSelector(".btn-2018check__input[title='" + groupName + "']")).click();
        sleep(2000);
    }

    public void getSelectedUserGroups(){
        userGroups.stream().forEach(group -> System.out.println(group.getText()));
    }

    @Step("Open user profile")
    public void openUserProfile() {
        profileLink.click();
        sleep(2000);
    }

    @Step("Click on Groups")
    public void clickOnGroupsBtn() {
        groupsBtn.click();
    }

    @Step("Click on Add Members")
    public void clickOnAddMembers() {
        addMembers.click();
    }

    @Step("Choose SAML method")
    public void chooseSAMLMethod() {
        wait.until(ExpectedConditions.elementToBeClickable(addSAML));
        addSAML.click();
    }

    @Step("Choose Built In method")
    public void chooseBuiltInMethod() {
        wait.until(ExpectedConditions.elementToBeClickable(addBuiltIn));
        addBuiltIn.click();
    }

    public void clickOnNewMember() {
        newMemberBtn.click();
    }

    @Step("Input First Name")
    public void inputFirstName(String firstName) {
        firstNameInput.sendKeys(firstName);
    }

    @Step("Input Last Name")
    public void inputLastName(String lastName) {
        lastNameInput.sendKeys(lastName);
    }

    @Step("Input Email")
    public void inputEmail(String email) {
        emailInput.sendKeys(email);
    }

    @Step("Input Username")
    public void inputUsername(String username) {
        usernameInput.sendKeys(username);
    }

    @Step("Input Password")
    public void inputPassword(String password) {
        passwordInput.sendKeys(password);
    }

    @Step("Select User Type {userTypeToBeSelected}")
    public void selectUserType(String userTypeToBeSelected){
        userTypeDrpDwn.click();
        userTypeDrpDwnOptions.stream().filter(option -> option.getText().contains(userTypeToBeSelected)).findFirst().get().click();
    }

    @Step("Select User Role {userRoleToBeSelected}")
    public void selectUserRole(String userRoleToBeSelected){
        userRoleDrpDwn.click();
        sleep(5000);
        userRoleDrpDwnOptions.stream().filter(option -> option.getText().contains(userRoleToBeSelected)).findFirst().get().click();
    }

    public String getUserName() {
        return userName.getText();
    }

    public List<String> getUserNames() {
        return userNames.stream().map(userName -> userName.getText()).collect(Collectors.toList());
    }

    public void clickOnSearchMemberDrpDwn() {
        searchMemberDrpDwn.click();
    }

    public void clickOnSearchMemberByUsername() {
        searchMemberByUsernameBtn.click();
    }

    @Step("Select search members by Username")
    public void selectSearchByUsername() {
        clickOnSearchMemberDrpDwn();
        clickOnSearchMemberByUsername();
    }

    @Step("Click on Add Members button")
    public void clickOnAddMemberBtn() {
        sleep(3000);
        wait.until(ExpectedConditions.elementToBeClickable(newMemberBtn));
        action.moveToElement(newMemberBtn)
                .click(newMemberBtn)
                .perform();
    }

    @Step("Click on Add button")
    public void clickOnAddBtn() {
        addBtn.click();
        sleep(2000);
    }

    @Step("Click on Next button")
    public void clickOnNextBtn() {
        nextBtn.click();
        sleep(2000);
    }

    @Step("Click on Members Tab")
    public void clickOnMembersTab() {
        membersTab.click();
        sleep(5000);
    }

    @Step("Input username {searchText} in search box")
    public void inputSearchText(String searchText) {
        searchBox.sendKeys(searchText);
        sleep(2000);
    }

    @Step("Verify that member is present")
    public void selectFirstMemberSearchResult() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".js-loader-wrap")));
        firstSearchResult.click();
        sleep(2000);
    }

    @Step("Click on New members from a file button")
    public void clickOnNewMembersFromFileBtn() {
        newMembersFromFileBtn.click();
    }

    @Step("Upload file")
    public void uploadFile(String test) {
        driver.findElement(By.cssSelector(".js-file-input")).sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\data\\upload_files\\"+test+".csv");
        sleep(2000);
    }

    @Step("Click on add file button")
    public void clickOnAddFile() {
        sleep(2000);
        addFileBtn.click();
    }

    @Step("Verify that field {fieldName} is marked erroneous")
    public void verifyErrorField(String fieldName){
        errorFields.stream().forEach(errorField -> System.out.println(errorField.getText()));
        assertThat(errorFields.stream().filter(errorField -> errorField.getText().trim().contains(fieldName.trim())).collect(Collectors.toList()).size()).isGreaterThan(0);
    }

    public void closeAddMembersPage(){
        closeAddMembersPage.click();
    }

    public void clickOnMemberOptions(){
        memberOptionsBtn.click();
    }

    @Step("Select {option} option for the member")
    public void selectMemberOption(String option){
        sleep(1000);
        this.clickOnMemberOptions();
        memberOptions.stream().filter(memberOption -> memberOption.getText().equalsIgnoreCase(option)).findFirst().get().click();
        wait.until(CustomCondition.waitForPageToLoad());
    }
/*
    @Step("Click on Add to Groups")
    public void clickOnAddToGroupsBtn(){
        addToGroupsBtn.click();
    }*/

    /*@Step("Search for group {groupName}")
    public void searchForGroup(String groupName) {
        addToGroupsPopupSearchBox.clear();
        addToGroupsPopupSearchBox.sendKeys(groupName);
        addToGroupsPopupSearchBox.sendKeys(Keys.ENTER);
        sleep(5000);
    }*/
/*
    @Step("Select the group {groupName} from the list")
    public void selectGroupFromTheListAndAdd(String groupName) {
        System.out.println(addToGroupsPopupGroupList.size());
        addToGroupsPopupGroupList.stream().filter(group -> group.getText().equalsIgnoreCase(groupName)).findFirst().get().click();
        addToGroupsPopupAddBtn.click();
    }*/

    @Step("Delete Member")
    public void clickOnDeleteMemberBtn(){
        deleteMemberBtn.click();
        sleep(1000);
    }

    @Step("Update User Type {userType}")
    public void updateUserType(String userType) {
        userTypeInputs.stream().filter(usernameInput -> usernameInput.getAttribute("aria-label").contains(userType)).findFirst().get().click();
        this.clickOnNextBtn();
    }

    @Step("Update Role to {newRole}")
    public void updateUserRole(String newRole){
        roleDrpDwn.click();
        roleTypesDrpDwnOpts.stream().filter(role -> role.getText().trim().equalsIgnoreCase(newRole)).findFirst().get().click();
        wait.until(CustomCondition.waitForPageToLoad());
    }

    @Step("Verify that user is NOT able to add new members")
    public void verifyThatAddMembersLinkNotDisplayed(){
        assertThat(this.addMembers_list.size()).isEqualTo(0);
    }

    @Step("Verify that newest members list is displayed on organization page")
    public void verifyThatNewestMembersListIsDisplayed(){
        assertThat(newestMembersList.stream().collect(Collectors.toList()).size()).isGreaterThanOrEqualTo(1);
    }

    @Step("Verify that latest content list is displayed on organization page")
    public void verifyThatLatestContentListIsDisplayed(){
        assertThat(latestContentList.stream().collect(Collectors.toList()).size()).isGreaterThanOrEqualTo(1);
    }
}
