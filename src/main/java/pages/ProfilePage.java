package pages;

import io.qameta.allure.Step;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.CustomCondition;

import static org.assertj.core.api.Assertions.assertThat;

public class ProfilePage extends HeaderPage {

    @FindBy(css = "#firstNameProperty .readOnly")
    WebElement profileFirstName;

    @FindBy(css = "#lastNameProperty .readOnly")
    WebElement profileLastName;

    @FindBy(css = "#organization_role_div .esriItemLinks,.role-name") // .role-name
    WebElement profileRole;

    @FindBy(css = ".organization_usertype #dijit__TemplatedMixin_1")
    WebElement profileUserType;

    @FindBy(css = "#editMenu span[widgetid='profileEditButton'] .dijitButtonNode")
    WebElement editProfileBtn;

    @FindBy(css = ".dijitInputInner[name='first_name']")
    WebElement editProfileFirstName;

    @FindBy(css = ".dijitInputInner[name='last_name']")
    WebElement editProfileFLastName;

    @FindBy(id = "profileBio")
    WebElement editProfileBioTxtArea;

    @FindBy(css = "#lowerProfileSaveButton_label")
    WebElement editProfileSaveBtn;

    public ProfilePage() {
        PageFactory.initElements(driver, this);
    }

    public String getMemberFirstName() {
        wait.until(CustomCondition.waitForPageToLoad());
        return profileFirstName.getText();
    }

    public String getMemberLastName() {
        wait.until(CustomCondition.waitForPageToLoad());
        return profileLastName.getText();
    }

    public String getMemberRole() {
        wait.until(CustomCondition.waitForPageToLoad());
        return profileRole.getText();
    }

    public String getMemberUserType(){
        wait.until(CustomCondition.waitForPageToLoad());
        return profileUserType.getText();
    }

    public String getExpectedUserRole() {
        String expectedRole = "";
        if(this.getMemberUserType().equalsIgnoreCase("Creator"))
            expectedRole = "EQL User";
        else if(this.getMemberUserType().equalsIgnoreCase("Viewer"))
            expectedRole = "Viewer";
        return expectedRole;
    }


    @Step("Click on Edit Profile for the member")
    public void clickOnEditProfile(){
        wait.until(ExpectedConditions.elementToBeClickable(editProfileBtn));
        editProfileBtn.click();
    }

    @Step("Update First Name for the member")
    public void updateFirstName(String newFirstName){
        editProfileFirstName.clear();
        editProfileFirstName.sendKeys(newFirstName);
    }

    @Step("Update Last Name for the member")
    public void updateLastName(String newLastName){
        editProfileFLastName.clear();
        editProfileFLastName.sendKeys(newLastName);
    }

    @Step("Update Bio for the member")
    public void updateBio(String bioText){
        editProfileBioTxtArea.clear();
        editProfileBioTxtArea.sendKeys(bioText);
    }

    @Step("Save the profile of the member")
    public void clickOnSaveBtn(){
        editProfileSaveBtn.click();
        sleep(4000);
    }

    @Step("Verify that first name is updated to {expectedFirstName}")
    public void verifyThatFirstNameIsUpdated(String expectedFirstName){
        assertThat(this.getMemberFirstName()).isEqualToIgnoringCase(expectedFirstName);
    }

    @Step("Verify that last name is updated to {expectedLastName}")
    public void verifyThatLastNameIsUpdated(String expectedLastName){
        assertThat(this.getMemberLastName()).isEqualToIgnoringCase(expectedLastName);
    }

    @Step("Verify that user role is updated to {expectedRole}")
    public void verifyUserRole(String expectedRole){
        sleep(2000);
        assertThat(this.getMemberRole().trim()).isEqualToIgnoringCase(expectedRole);
        sleep(1000);
    }

    @Step("Verify that user type is updated to {expectedUserType}")
    public void verifyUserType(String expectedUserType){
        sleep(1000);
        assertThat(this.getMemberUserType().trim()).isEqualToIgnoringCase(expectedUserType);
        sleep(1000);
    }

    @Step("Verify that default user role is updated to {expectedRole}")
    public void verifyDefaultUserRole(SoftAssertions softly, String expectedRole){
        softly.assertThat(this.getMemberRole().trim()).isEqualToIgnoringCase(expectedRole); //Should be EQL User for Creator
        sleep(1000);
    }


}
