package tests.smoke;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import pages.*;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Portal_Smoke_Test extends BaseTest {

    @Test
    public void portal_smoke_test_gallery() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        GalleryPage galleryPage = new GalleryPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        /*headerPage.corporateLogin(properties.getProperty("corpLoginId"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));*/
        headerPage.login();

        /**
         * Test Gallery
         */
        headerPage.clickOnGalleryTab();
        galleryPage.searchInGalleryFor("UNM");
        galleryPage.verifyThatGalleryItemsArePresent();
        galleryPage.searchInGalleryFor("Base Network Web App");
        galleryPage.verifyThatGreenTickIsDisplayed();
    }

    @Test
    public void portal_smoke_test_user_profile() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        GalleryPage galleryPage = new GalleryPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        /*headerPage.corporateLogin(properties.getProperty("corpLoginId"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));*/
        headerPage.login();

        /**
         * Test user profile
         */
        headerPage.clickOnUserProfileIcon();
        headerPage.clickOnMyProfile();
        profilePage.verifyUserRole("Administrator");
        profilePage.verifyUserType("Creator");
        headerPage.clickOnHomeTab();

    }

        @Test
    public void portal_smoke_test_groups() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        GalleryPage galleryPage = new GalleryPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();

        /**
         * Test Groups
         */

        headerPage.clickOnGroupsTab();
        groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.verifyThatCreateBtnIsNotDisplayed();
        groupsPage.searchForGroupOrContent("Environmental and Cultural Heritage");
        groupsPage.verifyThatGroupIsPresent("Environmental and Cultural Heritage");
        groupsPage.selectGroup("Environmental and Cultural Heritage");
        groupsPage.verifyThatGroupContentIsDisplayed();
    }

    @Test
    public void portal_smoke_test_map_viewer() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        GalleryPage galleryPage = new GalleryPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        /*headerPage.corporateLogin(properties.getProperty("corpLoginId"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));*/
        headerPage.login();

        /**
         * Test Map Viewer
         */

        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Organization");
        mapPage.inputLayerNameInSearchBox("Koala");
        mapPage.selectAndAddLayerToMap("Koala Protection Area");
        mapPage.clickOnSaveWebMapBtn();
        mapPage.clickOnSaveWebMapBtn();
        String mapName = "My Koala Map";//+ Utils.getRandomNumberInRange(1, 500);
        mapPage.inputWebMapName(mapName);
        mapPage.inputTags("#test");
        mapPage.saveWebMap();
        mapPage.goToContentPage();
        contentPage.verifyThatMapIsPresentOnContentPage("My Koala Map");
        // print is not included
    }

    @Test(dependsOnMethods = "portal_smoke_test_map_viewer")
    public void portal_smoke_test_content() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        GalleryPage galleryPage = new GalleryPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        /*headerPage.corporateLogin(properties.getProperty("corpLoginId"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));*/
        headerPage.login();

        /**
         * Test Content
         */

        headerPage.clickOnContentTab();
        contentPage.verifyThatMapIsPresentOnContentPage("My Koala Map");
        contentPage.searchAndDeleteMapOnContentPage("My Koala Map");
        headerPage.logout();

        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnContentTab();
        contentPage.clickOnMyGroupsTab();
        List<String> expectedFilters = new ArrayList<>();
        expectedFilters.add("Item Type");
        expectedFilters.add("Location");
        expectedFilters.add("Date Modified");
        expectedFilters.add("Date Created");
        expectedFilters.add("Tags");
        expectedFilters.add("Shared");
        expectedFilters.add("Status");
        expectedFilters.add("Collaboration");
        contentPage.verifyThatFiltersAreDisplayed(expectedFilters);
        contentPage.searchMapOnContentPage("Network");
        contentPage.selectFilter("Item Type", "Layers->Map Image Layers");
        contentPage.verifyThatMapIsPresentOnContentPageMyGroupsTab("Base Network");
        contentPage.openMapOnMyGroupsTab("Base Network");
        contentPage.webMapPage.verifyThatLayerIsPointingToCorrectEnv(properties.getProperty("environment"));
        contentPage.webMapPage.clickOnViewLink();
        contentPage.webMapPage.verifyThatGIS_REST_DirectoryIsOpened("ArcGIS REST Services Directory");
    }

    @Test
    public void portal_smoke_test_organization() {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();

        /**
         * Test Organization
         */
        headerPage.clickOnOrgTab();
        //organizationPage.verifyThatAddMembersLinkNotDisplayed(); //Only for non admin users
        organizationPage.verifyThatNewestMembersListIsDisplayed();
        organizationPage.verifyThatLatestContentListIsDisplayed();

    }
}
