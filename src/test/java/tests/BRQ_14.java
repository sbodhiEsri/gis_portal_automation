package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.HeaderPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_14 extends BaseTest {

    @Test(description = "Login Page – Authenticate Successfully on Enterprise Portal")
    @Story("BRQ 14")
    public void validate_user_can_login_in_portal() {

        HeaderPage headerPage = new HeaderPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.logout();
    }
}
