package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_13 extends BaseTest {

    @Test(description = "UGIS-2893 Verify that user can configure the display of feature search results in tabular form using Portal")
    @Story("BRQ 13")
    public void UGIS_2893_validate_user_can_configure_display_of_feature_search_results_in_tabular_form_in_Portal() {

        HeaderPage headerPage = new HeaderPage();
        ContentPage contentPage = new ContentPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Fish Habitat");
        contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.clickOnFilterForLayer("Fish Habitat - Fish habitat area");
        mapPage.selectFieldToFilter("Currency");
        mapPage.inputValueForSelectedField("Apr 2009");
        mapPage.clickOnFilterButton();
        mapPage.clickOnShowAttributeTableForLayer("Fish Habitat - Fish habitat area");
        mapPage.clickOnAttributeTableHeaderCurrency();
        mapPage.clickOnSort("Ascending");
        String currency_asc_value = mapPage.getFirstValueForFieldCurrency();
        mapPage.verifyThatValuesAreSortedInAscForFieldCurrency();
        mapPage.clickOnAttributeTableHeaderCurrency();
        mapPage.clickOnSort("Descending");
        String currency_desc_value = mapPage.getFirstValueForFieldCurrency();
        mapPage.verifyThatValuesAreSortedInDescForFieldCurrency();
        mapPage.verifyAscDescValuesAreSameForAttributeTableColumn(currency_asc_value, currency_desc_value);
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuFilter();
        mapPage.clickOnRemoveFilterBtn();
        mapPage.clickOnAttributeTableHeaderCurrency();
        mapPage.clickOnSort("Descending");
        currency_desc_value = mapPage.getFirstValueForFieldCurrency();
        mapPage.verifyAscDescValuesAreNOTSameForAttributeTableColumn(currency_asc_value, currency_desc_value);
    }
}
