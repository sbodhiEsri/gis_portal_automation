package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104k extends BaseTest {

    @Test(description = "An administrator user can successfully change the visibility range on each Pop up associated with each feature layer")
    @Story("BRQ 104k")
    public void specify_popup_scale_successfully_specify_the_scale_at_which_user_assess_data_at_different_feature_types_and_classes() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("CurrentPoultryFarms");
        contentPage.webMapPage.clickOnOpenInMapViewer();

        mapPage.selectLayer("CurrentPoultryFarms");
        //mapPage.clickOnTransparency("CurrentPoultryFarms");
        //mapPage.setTransparency(50);
        //mapPage.clickOnNoteOnMapLayer();
        //mapPage.verifyThatPopupIsPresentOnMap();

        mapPage.clickOnSetVisibilityRange("CurrentPoultryFarms");
        mapPage.selectVisibilityOption("States / Provinces");
        mapPage.verifyVisibilityIsOutOfRangeForLayer("CurrentPoultryFarms");
        mapPage.clickOnZoomToLayer("CurrentPoultryFarms");
        mapPage.verifyVisibilityIsInRangeForLayer("CurrentPoultryFarms");
        //mapPage.pan4();
        mapPage.clickOnNoteOnMapLayer();
        mapPage.verifyThatPopupIsPresentOnMap();
    }
}
