package tests.webapp;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.*;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_18b extends BaseTest {

    @Test(description = "An ArcGIS user can successfully export data in multiple formats.")
    @Story("BRQ 18b")
    @Description("Test a : Export Data  – Successfully export data from WebApp for ArcGIS.")
    public void test_a_export_data_successfully_export_data_from_WebApp_for_ArcGIS() {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        webAppPage.cleanDownloadDirectory();
        webAppPage.clickOnSelectTab();
        webAppPage.clickOnSelectBtn();
        webAppPage.drawAnAreaOnTheMap(-150,-160,150,160);
        int expectedCount = webAppPage.clickOnMoreOptionsForLayerWithCountGreaterThanZero();
        webAppPage.selectOptionToExportInSelectWidget("Export to CSV");
        webAppPage.verifyThatFileIsDownloaded("features.csv");
        webAppPage.verifyTheCount(expectedCount+1, webAppPage.getNoOfRecordsInCSVFile("features.csv"));

        expectedCount = webAppPage.clickOnMoreOptionsForLayerWithCountGreaterThanZero();
        webAppPage.selectOptionToExportInSelectWidget("Export to feature collection");
        webAppPage.verifyThatFileIsDownloaded("features.json");
        webAppPage.verifyTheCount(expectedCount, webAppPage.getNoOfFeaturesInJsonFile("features.json"));

        expectedCount = webAppPage.clickOnMoreOptionsForLayerWithCountGreaterThanZero();
        webAppPage.selectOptionToExportInSelectWidget("Export to GeoJSON");
        webAppPage.verifyThatFileIsDownloaded("features.geojson");
        webAppPage.verifyTheCount(expectedCount, webAppPage.getNoOfFeaturesInJsonFile("features.geojson"));
    }

    @Test(description = "An ArcGIS user can export data in multiple formats.")
    @Story("BRQ 18b")
    public void test2_BRQ_18b_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();


        webAppPage.clickOnPrintWidgetTab();
        webAppPage.clickOnPrintLayoutDrpDwn();
        webAppPage.selectPrintDropDownOption("A4 Landscape");

        webAppPage.clickOnPrintFormatDrpDwn();
        webAppPage.selectPrintDropDownOption("PDF");

        webAppPage.clickOnAdvancedBtn();
        webAppPage.selectPreserveMapExtent();

        webAppPage.clickOnPrintBtn();
        webAppPage.verifyThatPrintProgressBarIsDisplayed();
        webAppPage.verifyThatLinkForThePrintJobIsDisplayed();
        webAppPage.clickOnPrintJobLink();
        webAppPage.verifyThatPDFFileIsOpenedInNewWindow();
        webAppPage.clickOnClearPrints();
        webAppPage.verifyThatLinkForThePrintJobIsNotDisplayed();
    }
}
