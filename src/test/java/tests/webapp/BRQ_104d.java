package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104d extends BaseTest {

    @Test(description = "An ArcGIS user can successfully loacte the the layer widget to understand the Symbology in the  web app")
    @Story("BRQ 104d")
    public void test_BRQ_104d_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");//Restricted
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        webAppPage.clickOnLegendTab();
        int previousNoOfLegends = webAppPage.getWebAppLegendIcons().size();
        //webAppPage.clickOnZoomIn(10);
        webAppPage.searchOnMap2("128 Charlotte St, Brisbane City");
        webAppPage.closeSearchPopup();
        webAppPage.takeScreenshot("Map Legends Actual");
        webAppPage.getWebAppLegendIcons();
        webAppPage.verifyThatLegendsAreChangedOnZoomIn(previousNoOfLegends);
        webAppPage.compareImage("Map Legends Expected", "Map Legends Actual");
        previousNoOfLegends = webAppPage.getWebAppLegendIcons().size();
        webAppPage.clickOnZoomOut(10);
        webAppPage.verifyThatLegendsAreChangedOnZoomOut(previousNoOfLegends);
    }
}
