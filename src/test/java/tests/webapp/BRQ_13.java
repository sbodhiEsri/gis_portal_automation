package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BRQ_13 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully view search results, order and filter search results.")
    @Story("BRQ 13")
    public void test_BRQ_13_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Opening an attribute table
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Managed Environment"); //"Managed Environment" Biosecurity
        //webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        //webAppPage.clickOnZoomToForLayer();
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnViewInAttributeTableForLayer();
        webAppPage.verifyThatAttributeTableIsDisplayed();
        /*webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Filter");
        webAppPage.webAppAttributeTable.addFilterExpression();
        webAppPage.webAppAttributeTable.selectFilterField("Township"); // Biosecurity zone
        webAppPage.webAppAttributeTable.provideFieldValue("CLERMONT 4721"); // MONTO 4630 // Electric ant biosecurity zone EA02
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesAreOne();*/

        /**
         * Verify Show/Hide columns in attribute table
         */
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Show/Hide columns");
        List<String> columnsToHide =  new ArrayList<>();
        columnsToHide.add("Comments");
        columnsToHide.add("Owner");
        columnsToHide.add("Charged Service");
        webAppPage.webAppAttributeTable.deselectColumnsFromAttributeTable(columnsToHide);
        webAppPage.webAppAttributeTable.verifyThatHeadersAreNotDisplayedInAttributeTable(columnsToHide);


        /**
         * Verify sorting a column
         */
        webAppPage.webAppAttributeTable.sortColumn("Inserted By", "descending");
        List<String> valuesAfterSort = webAppPage.webAppAttributeTable.getAllValuesForColumn("Inserted By");
        webAppPage.webAppAttributeTable.verifyThatColumnIsSortedInDescendingOrder(valuesAfterSort);
        webAppPage.webAppAttributeTable.sortColumn("Inserted By", "ascending");
        valuesAfterSort = webAppPage.webAppAttributeTable.getAllValuesForColumn("Inserted By");
        webAppPage.webAppAttributeTable.verifyThatColumnIsSortedInAscendingOrder(valuesAfterSort);


        /**
         * Verify changing column positions
         */
        webAppPage.webAppAttributeTable.swapColumnPositionsFor("Township", "Location Address");
        webAppPage.webAppAttributeTable.verifyThatColumnPositionIsChanged("Township", "Location Address");

        /**
         * Verify filter by map extent
         */
        webAppPage.searchOnMap2("Brisbane City Hall");
        webAppPage.closeSearchPopup();
        int previousNoOfFeatures = webAppPage.webAppAttributeTable.getTotalNoOfFeaturesInAttributeTable();
        webAppPage.webAppAttributeTable.clickOnButtonInAttributeTable("Filter by map extent");
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesDisplayedInAttributeTableAreDecreased(previousNoOfFeatures);
        webAppPage.clickOnEnableCoordinateLocateBtn();
        webAppPage.clickOnTheCentreOfTheMap();
        webAppPage.verifyThatEsriGreenPinIsDisplayedOnTheMap();
        webAppPage.verifyTheCoordinates("153.024 -27.469 Degrees");
    }

}
