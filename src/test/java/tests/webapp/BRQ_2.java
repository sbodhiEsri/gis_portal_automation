package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_2 extends BaseTest {

    @Test(description = "Verify that An ArcGIS user can successfully locate, create and move a feature using the Environment and Cultural Heritage Restricted Data Web App.")
    @Story("BRQ 2")
    public void  successfully_locate_create_and_move_a_feature_using_the_Environment_and_Cultural_Heritage_Restricted_Web_App() {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        webAppPage.clickOnSearchInDrpDwn();
        webAppPage.selectSearchInOption("QLD Lot Plan");
        webAppPage.searchOnMap2("4RP34046");
        webAppPage.closeSearchPopup();
        webAppPage.clickOnEditTab();
        webAppPage.selectEditNoteOption("Points");
        webAppPage.drawPointOnTheMap();
        webAppPage.inputPointNoteTitle("Property Access");
        webAppPage.inputPointNoteDescription("New access gate installed along front fence");
        webAppPage.clickOnPointNoteSaveBtn();
        webAppPage.clickOnPointNoteCloseBtn();
        String previousX = webAppPage.getXLocationOfPointNote();
        String previousY = webAppPage.getYLocationOfPointNote();
        webAppPage.movePointNoteToNewLocation();
        webAppPage.verifyThatPointNoteIsMovedToNewLocation(previousX, previousY);
        webAppPage.clickOnPointNoteDeleteBtn();
    }
}
