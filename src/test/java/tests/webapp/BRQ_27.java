package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.List;
import java.util.Properties;

import static pages.BasePage.verifyTwoLists;

public class BRQ_27 extends BaseTest {

    @Test(description = "Validate that the feature symbology used in the Base Network Web app is identical to the Base Network Web map.")
    @Story("BRQ 27")
    public void successfully_validate_that_the_feature_symbology_used_in_the_Base_Network_Web_app_is_identical_to_the_map() {

        HeaderPage headerPage = new HeaderPage();
        ContentPage contentPage = new ContentPage();
        MapPage mapPage = new MapPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        /**
         * Open Web map and turn on the layers to get legend icons
         */

        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Base Network Web Map");
        contentPage.webMapPage.clickOnOpenInMapViewer();

        mapPage.sleep(15000);
        //mapPage.clickOnDetails();
        /*mapPage.clickOnBookmarks();
        mapPage.clickOnBookmarkWithName("Mackay");*/
        mapPage.clickOnShowMapContentBtn();
        mapPage.deselectLayers();
        mapPage.selectLayer("Electric Network");
        mapPage.selectLayer("DNRME DCDB");
        mapPage.sleep(5000);
        mapPage.clickOnShowLegendOfMap();
        mapPage.sleep(5000);
        List<String> mapLegendItems = mapPage.getMapLegendIcons();

        /**
         * Open WebApp and turn on the layers to get legend icons
         */
        mapPage.goToContentPage();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Base Network Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Network Web App");
        /*webAppPage.clickOnBookmarkTab();
        webAppPage.selectBookMark("Mackay");*/
        webAppPage.clickOnLayerListTab();
        webAppPage.turnOffAllLayersOfTheWebApp();
        webAppPage.turnOnLayer("Electric Network");
        webAppPage.turnOnLayer("DNRME DCDB");
        webAppPage.clickOnLegendTab();
        List<String> webAppItems = webAppPage.getWebAppLegendIcons();
        verifyTwoLists(mapLegendItems, webAppItems);
    }
}
