package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BRQ_Bookmarks extends BaseTest {

    @Test(description = "An ArcGIS user can successfully create a bookmark in a web app .")
    @Story("BRQ ")
    public void test_BRQ__Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Turn all layers off first
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.turnOffAllLayersOfTheWebApp();

        /**
         * Testing bookmark
         */
        webAppPage.clickOnBookmarkTab();
        List<String> bookmarks = new ArrayList<>();
        bookmarks.add("Mackay");
        bookmarks.add("Cairns");
        bookmarks.add("Barron Gorge");
        webAppPage.verifyTheBookmarksArePresent(bookmarks);

        webAppPage.cleanScreenshotDirectory();
        webAppPage.selectBookMark("Mackay");
        webAppPage.takeScreenshot("Mackay Actual");
        webAppPage.compareImage("Mackay", "Mackay Actual");

        /*webAppPage.cleanScreenshotDirectory();
        webAppPage.selectBookMark("Barron Gorge");
        webAppPage.takeScreenshot("Barron Gorge Actual");
        webAppPage.compareImage("Barron Gorge", "Barron Gorge Actual");

        webAppPage.cleanScreenshotDirectory();
        webAppPage.selectBookMark("Cairns");
        webAppPage.takeScreenshot("Cairns Actual");
        webAppPage.compareImage("Cairns", "Cairns Actual");*/

        /**
         * Create a bookmark
         */
        webAppPage.clickOnHomeBtn();
        //webAppPage.clickOnZoomIn(4);
        webAppPage.searchOnMap("Brisbane");
        webAppPage.closeSearchPopup();
        webAppPage.clickOnAddBookmarkBtn();
        webAppPage.inputBookmarkNameAs("Test Bookmark");
        webAppPage.takeScreenshot("Bookmark Expected");
        webAppPage.clickOnHomeBtn();
        webAppPage.selectBookMark("Test Bookmark");
        webAppPage.takeScreenshot("Bookmark Actual");
        webAppPage.compareImage("Bookmark Expected", "Bookmark Actual");

        /**
         * Delete Bookmark
         */
        webAppPage.deleteBookMark("Test Bookmark");
    }
}
