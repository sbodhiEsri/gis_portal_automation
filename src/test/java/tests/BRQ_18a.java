package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.DataProvider.MapDataProviderClass;
import utils.LoadProperties;

import java.util.Map;
import java.util.Properties;

public class BRQ_18a extends BaseTest {

    @Test(description = "An ArcGIS user can successfully Import one of listed GIS file data formats", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    @Story("BRQ 18a")
    @Description("Test ab : Add data into portal from content tab")
    public void test_ab_import_data_Successfully_add_data_into_Portal_through_locally_stored_data_in_content_tab  (Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
        contentPage.clickOnAddItem();
        contentPage.clickOnAddItemFromComputer();
        contentPage.chooseFileToUpload(input.get("File Name"));
        String layer_name = "BRQ 18 " + input.get("File Type") + " Test2";
        contentPage.inputItemTitle(layer_name);
        contentPage.inputItemTags("#Test");
        contentPage.clickOnAddItemBtn();
        //contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnDetails();
        //mapPage.clickOnShowMapContentBtn();
        headerPage.clickOnContentTab();
        contentPage.verifyThatLayerIsPresentOnContentPage(layer_name, input.get("Content Count"));
        contentPage.searchAndDeleteMapOnContentPage(layer_name);

        if(input.get("File Type").equalsIgnoreCase("Shapefile")
        || input.get("File Type").equalsIgnoreCase("CSV"))
            contentPage.verifyThatSuccessMsgIsDisplayed("Items successfully deleted.");
        else
            contentPage.verifyThatSuccessMsgIsDisplayed("Item successfully deleted.");

        contentPage.logout();
    }

    @Test(description = "An ArcGIS user can successfully Import one of listed GIS file data formats",  dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    @Story("BRQ 18a")
    @Description("Test aa : Add data into portal from map tab")
    public void test_aa_import_data_Successfully_add_data_into_Portal_through_locally_stored_data_in_map (Map<String,String> input) {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnAddLayerFromFile();
        mapPage.uploadFileToBeAddedAsLayer(input.get("File Name"));
        mapPage.clickOnImportLayerBtn();
        mapPage.verifyThatImportLayerErrorMessageIsNotDisplayed();
        mapPage.clickOnDone();
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.verifyLayerIsDisplayedInContentPane(input.get("Layer Name"));
        //mapPage.clickOnZoomToLayer(input.get("Layer Name"));
        //mapPage.verifyLayerIsDisplayedOnTheMap("Depot final");
        mapPage.logout();
    }

    @Test(description = "An ArcGIS user can successfully Import one of listed GIS file data formats", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    @Story("BRQ 18a")
    @Description("Test ab : Add CSV files into portal from content tab")
    public void test_aa_import_data_Successfully_add_data_into_Portal_through_locally_stored_data_in_content_tab  (Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
        contentPage.clickOnAddItem();
        contentPage.clickOnAddItemFromComputer();
        contentPage.chooseFileToUpload(input.get("File Name"));
        String layer_name = "BRQ 18 " + input.get("File Type") + " Test2";
        contentPage.inputItemTitle(layer_name);
        contentPage.inputItemTags("#Test");
        contentPage.clickOnTableOnlyOptionForLocateFeatureBy();
        contentPage.clickOnAddItemBtn();
        //contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnDetails();
        //mapPage.clickOnShowMapContentBtn();
        headerPage.clickOnContentTab();
        contentPage.verifyThatLayerIsPresentOnContentPage(layer_name, input.get("Content Count"));
        contentPage.searchAndSelectMap(layer_name);
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.verifyLayerIsDisplayedInContentPane(layer_name);
        mapPage.goToContentPage();
        contentPage.searchAndDeleteMapOnContentPage(layer_name);
        contentPage.verifyThatSuccessMsgIsDisplayed("Items successfully deleted.");
        contentPage.logout();
    }
}
