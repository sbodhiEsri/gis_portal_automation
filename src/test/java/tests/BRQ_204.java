package tests;

import org.testng.annotations.Test;
import pages.*;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_204 extends BaseTest {

    @Test(description = "A registered user should be able to successfully view UNM data in the EQL QLD service extent in a portal service")
    public void validate_user_is_able_to_view_UNM_data_in_the_EQL_QLD_service_extent_in_a_portal_service () {

        HeaderPage headerPage = new HeaderPage();
        ContentPage contentPage= new ContentPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Base Network");
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.clickOnDefaultExtentBtn();
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.expandLayerInContentPane("Base Network");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Feeder");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Distribution Subnet Line");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Transmission Subnet Line");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Subtransmission Subnet Line");
    }
}

