package tests;

import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;
import utils.Utils;

import java.util.Properties;

public class BRQ_104h_Create_Edit_Maps extends BaseTest {

    @Test(description = "UGIS-2207 Verify that Admin User is able to create a new map with layers and notes")
    public void UGIS_2207_Validate_Admin_can_create_map_with_layers_Browse_Living_Atlas_Layers() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Create new map with layers and notes");

        // reportLog("Click on Organization Tab", test);
        headerPage.clickOnMapTab();

        //reportLog("Click on Add Members", test);
        mapPage.clickOnBaseMapBtn();
        mapPage.selectBaseMap("Dark Gray Canvas");

        //reportLog("Choose SAML method", test);
        mapPage.clickOnAddBtn();

        // reportLog("Click on Next button", test);
        mapPage.clickOnBrowseLivingAtlasLayers();

        // reportLog("Click on New members from a file button", test);
        mapPage.selectAndAddLayerToMap("A Children’s Map");

        //  reportLog("Click on Add file button", test);
        mapPage.clickOnAddBtn();
        mapPage.clickOnAddMapNotes();
        mapPage.clickOnCreateMapNotes();

        // reportLog("Click Next", test);
        mapPage.addStickPinOnMap();

        mapPage.clickOnSaveWebMapBtn();
        String mapName = "Test Map " + Utils.getRandomNumberInRange(1, 500);
        mapPage.inputWebMapName(mapName);
        mapPage.inputTags("#test");
        mapPage.saveWebMap();

        mapPage.goToContentPage();
        contentPage.searchAndSelectMap(mapName);
        headerPage.logout();
    }

//    @Test(description = "UGIS-2178 Verify that Admin User can create layers to a map and save to my content - Add Layer from Web")
    public void UGIS_2178_Validate_Admin_can_create_map_with_layers_add_layer_from_web() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Create new map with layers and notes");

        // reportLog("Click on Organization Tab", test);
        headerPage.clickOnMapTab();

        //reportLog("Click on Add Members", test);
        mapPage.clickOnBaseMapBtn();
        mapPage.selectBaseMap("Dark Gray Canvas");

        //reportLog("Choose SAML method", test);
        mapPage.clickOnAddBtn();

        // reportLog("Click on Next button", test);
        mapPage.clickOnAddLayerFromWeb();

        // reportLog("Click on New members from a file button", test);
        mapPage.inputWebLayerURL("http://");

        //  reportLog("Click on Add file button", test);
        mapPage.clickOnAddLayerBtn();
        mapPage.logout();
    }

    @Test(description = "UGIS_2210 Verify that Admin / Creator can edit an existing maps with multiple layers - Edit a specific layer")
    public void UGIS_2210_Validate_Admin_can_edit_existing_map_with_multiple_layers() throws InterruptedException {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Test User Test Map");
        contentPage.webMapPage.clickOnOpenInMapViewer();

        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();

        mapPage.clickOnTransparency("World Street Map");
        mapPage.setTransparency(50);

        mapPage.clickOnSetVisibilityRange("World Street Map");
        mapPage.selectVisibilityOption("Metropolitan Area");

        mapPage.clickOnMoveLayerDown("World Street Map");

        mapPage.clickOnMoveLayerUp("World Street Map");

        mapPage.renameLayer("World Street Map", "New Layer2");
        mapPage.verifyLayerIsDisplayedInContentPane("New Layer2");

        mapPage.clickOnCopy("Plantlife's Important Plant Areas"); // Protected plants flora survey trigger map
        mapPage.verifyLayerIsDisplayedInContentPane("Plantlife's Important Plant Areas - copy");

        mapPage.removeLayer("New Layer2");
        mapPage.verifyLayerIsRemoved("New Layer2");

        mapPage.clickOnHideInLegend("Plantlife's Important Plant Areas");

        mapPage.clickOnConfigureAndShowPopup("Plantlife's Important Plant Areas");

        mapPage.clickOnManageLabels("Plantlife's Important Plant Areas");

        mapPage.clickOnShowItemDetails("Plantlife's Important Plant Areas");
        mapPage.verifyThatLayerIsOpenedInNewWindow("Plantlife's Important Plant Areas");

        mapPage.setRefreshIntervalForLayer("Plantlife's Important Plant Areas", "10");
        mapPage.logout();
    }

    @Test(description = "UGIS_2209 Verify that Admin / Creator can edit an existing map - Turn Layers on and off")
    public void UGIS_2209_Validate_Admin_can_edit_existing_maps_Turn_Layers_on_off() throws InterruptedException {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.myOrganization.selectFilterCategory("Geography and Location");
        contentPage.searchAndSelectMap("Demo EQL Fire Response");
        contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.selectLayers();
        mapPage.deselectLayers();
        mapPage.verifyThatNoLayersAreVisible();
        mapPage.selectLayers();
        mapPage.verifyThatAllLayersAreVisible();
        mapPage.clickOnSaveAsWebMapBtn();
        String mapName = "Test Map " + Utils.getRandomNumberInRange(1, 500);
        mapPage.inputWebMapName(mapName);
        mapPage.inputTags("#test");
        mapPage.saveWebMap();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap(mapName);
        headerPage.logout();
    }

    @Test(description = "UGIS-2205 Verify that Admin/Creator can bookmark maps with layers - My Organisation")
    public void UGIS_2205_Validate_Admin_can_bookmark_maps_with_layers_My_Organisation() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.myOrganization.selectFilterItemType("Layers");
        contentPage.myOrganization.selectFilterItemType("Imagery Layers");
        contentPage.myOrganization.selectMap("QldBase_QgovSispUsers");
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.findPlaceOnMap("Brisbane");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Brisbane");
        mapPage.cleanDirectory();
        mapPage.takeScreenshot("Before Bookmark");
        mapPage.clickOnBookmarks();
        mapPage.clickOnAddBookmark();
        mapPage.inputBookmarkName("Brisbane");
        mapPage.closeBookmark();
        mapPage.clickOnZoomOut(5);
        mapPage.clickOnBookmarks();
        mapPage.verifyThatBookMarkIsPresent("Brisbane");
        mapPage.clickOnBookmarkWithName("Brisbane");
        mapPage.takeScreenshot("After Bookmark");
        mapPage.compareImage("Before Bookmark", "After Bookmark");
        mapPage.goToContentPage();
        headerPage.logout();
    }
}
