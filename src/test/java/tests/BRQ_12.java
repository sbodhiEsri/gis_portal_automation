package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_12 extends BaseTest {

    @Test(description = "UGIS-2891 Validate User can export search results in Portal")
    @Story("BRQ 12")
    public void UGIS_2891_validate_user_can_export_search_results_in_Portal() {

        HeaderPage headerPage = new HeaderPage();
        ContentPage contentPage = new ContentPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Fish Habitat");

    }
}
