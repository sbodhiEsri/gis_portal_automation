package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104f extends BaseTest {

    @Test(description = "UGIS-2918")
    @Story("BRQ 104f")
    public void UGIS_2918_Validate_administrator_can_set_symbology_and_colour_for_layers_in_Portal() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Demo EQL Fire Response"); //Test Map 82
        contentPage.webMapPage.clickOnOpenInMapViewer();

        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.clickOnChangeStyleForLayer("QLD Bushfire Current Incidents");
        mapPage.clickOnSelectBtnForLocation();
        mapPage.clickOnOptionBtnForLocation();
        mapPage.clickOnSymbolsLink();
        String expectedSymbolImage = mapPage.chooseSymbolInRowAndColumn("3", "3");
        mapPage.setSymbolSizeInPixel("35");
        mapPage.clickOnOkOnSymbolChangePopup();
        mapPage.clickOnOK();
        mapPage.clickOnDone();
        mapPage.deselectLayers();
        mapPage.selectLayer("QLD Bushfire Current Incidents");
        mapPage.clickOnZoomToLayer("QLD Bushfire Current Incidents");
        mapPage.verifySymbolIsChangedTo(expectedSymbolImage);
        mapPage.verifySizeOfTheSymbolIsChangedTo("35");
    }
}
