package tests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import nu.pattern.OpenCV;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.testng.annotations.Test;
import org.opencv.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

public class RestAssuredAPI {

    @Test
    public void test() {

        RestAssured.useRelaxedHTTPSValidation();
        Response response = given()
                .contentType("multipart/form-data")
                .multiPart("username", "portaladmin")
                .multiPart("password", "LxhTvkHYP47cwjK")
                .multiPart("f", "json")
                .multiPart("referer", "https")
                .when()
                .post("https://dev.gis.energyqonline.com.au/arcgis/sharing/rest" + "/generateToken");
        String token = response.path("token");
        System.out.println(token);

        ValidatableResponse response2 = given()
                .contentType("multipart/form-data")
                .multiPart("token", token)
                .multiPart("f", "json")
                .param("q", "First")
                .param("num", "100")
                .when()
                .post("https://dev.gis.energyqonline.com.au/arcgis/sharing/rest" + "/community/users").then();
        System.out.println(response2.toString());
        Map<String, ?> jsonMap = response2.extract()
                .jsonPath().get("");
        System.out.println(jsonMap);
        ArrayList<Map<String, String>> list = (ArrayList<Map<String, String>>) jsonMap.get("results");
        System.out.println(list);
        for (Map<String, String> map : list) {
            String username = map.get("username");
            Response response3 = given()
                    //.contentType("multipart/form-data")
                    .multiPart("token", token)
                    .multiPart("f", "json")
                    .when()
                    .post("https://dev.gis.energyqonline.com.au/arcgis/sharing/rest" + "/community/users/" + username + "/delete");
            System.out.println(response3.asString());
        }
    }


    @Test
    public void test2(){

        try {
            BufferedImage expectedImage = ImageIO.read(new File(System.getProperty("user.dir") + "\\Screenshots\\Expected.png"));
            BufferedImage actualImage = ImageIO.read(new File(System.getProperty("user.dir") + "\\Screenshots\\Actual.png"));

            Mat img = Imgcodecs.imread(System.getProperty("user.dir") + "\\Screenshots\\Expected.png", Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
            int erosion_size = 5;
            Mat element  = Imgproc.getStructuringElement(
                    Imgproc.MORPH_CROSS, new Size(2 * erosion_size + 1, 2 * erosion_size + 1),
                    new Point(erosion_size, erosion_size)
            );
            Imgproc.erode(img, img, element);

            BytePointer outText;

            tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();
            // Initialize tesseract-ocr with English, without specifying tessdata path
            if (api.Init("C:\\Users\\ra097\\IdeaProjects\\GIS_Automation_Allure", "eng") != 0) {
                System.err.println("Could not initialize tesseract.");
                System.exit(1);
            }

            // Open input image with leptonica library
            lept.PIX image = pixRead(System.getProperty("user.dir") + "\\Screenshots\\Actual.png");
            api.SetImage(image);
            // Get OCR result
            outText = api.GetUTF8Text();
            System.out.println("OCR output:\n" + outText.getString());

            // Destroy used object and release memory
            api.End();
            outText.deallocate();
            pixDestroy(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
