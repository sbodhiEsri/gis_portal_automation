package tests;

import com.relevantcodes.extentreports.ExtentTest;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;
import utils.Utils;

import java.util.Properties;

public class BRQ_15 extends BaseTest {

    @Test(description = "UGIS-2914 Verify that User can access help documentation in Portal")
    @Story("BRQ 15")
    public void UGIS_2914_validate_user_can_access_help_documentation_in_portal() {

        HeaderPage headerPage = new HeaderPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnUserProfileIcon();
        headerPage.clickOnHelp();
        headerPage.verifyThatHelpDocIsOpenedInNewWindow();
        headerPage.logout();
    }
}
