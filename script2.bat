 set projectLocation=%~dp0
 cd %projectLocation%
 taskkill /f /im chromedriver.exe
 call mvn clean test
 echo on
 call mvn pre-site | allure generate ./target/allure-results --clean -o allure-report
 echo on
 pause